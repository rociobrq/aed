#include <iostream>
using namespace std;

/* Definición de clase */
#include "Vector.h"

/* Constructores */
Vector::Vector(){
    int *vector;
    int cantidad;

}
Vector::Vector(int num){
    this->vector = NULL;
    this->cantidad = num;
}

/* Métodos */
void Vector::inicializar(){
    /* Se genera el vector de largo num (número ingresado) */
    this->vector = new int[this->cantidad];
}

void Vector::agregar_elemento(int elemento, int index){
    /* Se añaden elementos al vector */
    this->vector[index] = elemento;
}

void Vector::metodo_seleccion(){
    int num = this->cantidad;
    int menor, k;

    /* Algoritmo de orden */
    for(int i = 0; i <= num - 2; i++){
        menor = vector[i];
        k = i;

        /* Se revisa si en el resto de las posiciones hay un número menor al
        actualmente revisado. */
        for (int j = i + 1; j <= num - 1; j++){
            if(vector[j] < menor){
                /* Se almacena el número más pequeño del grupo y su índice */
                menor = vector[j];
                k = j;
            }
        }

        /* Intercambio de número actual con número menor obtenido. Si no se
        encuentra un número más bajo, no se generan cambios */
        vector[k] = vector[i];
        vector[i] = menor;
    }
}

void Vector::metodo_qsort(){
    int num = this->cantidad;
    int tope, ini, fin, pos;
    int pilamenor[num];
    int pilamayor[num];
    int izq, der, aux, band;

    tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = num - 1;

    while(tope >= 0){
        ini = pilamenor[tope];
        fin = pilamayor[tope];
        tope = tope - 1;

        /* Reduce */
        izq = ini;
        der = fin;
        pos = ini;
        band = true;

        while(band == true){
            /* Se compara valor actual con elementos desde der a izq */
            while((vector[pos] <= vector[der]) && (pos != der)){
                der = der - 1;
            }

            /* No se genera cambio en vector. No hay elementos menores al actual
            que indica variable pos */
            if(pos == der){
            band = false;
            }

            else {
                /* Intercambio */
                aux = vector[pos];
                vector[pos] = vector[der];
                vector[der] = aux;
                pos = der;

                /* Se compara valor con elementos desde izq a der */
                while((vector[pos] >= vector[izq]) && (pos != izq)){
                    izq = izq + 1;
                }

                /* No hay elementos mayores al actual que indica variable pos */
                if(pos == izq){
                    band = false;
                }

                else{
                    /* Intercambio */
                    aux = vector[pos];
                    vector[pos] = vector[izq];
                    vector[izq] = aux;
                    pos = izq;
                }
            }
        }

        if(ini < (pos - 1)){
            tope = tope + 1;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos - 1;
        }

        if(fin > (pos + 1)){
            tope = tope + 1;
            pilamenor[tope] = pos + 1;
            pilamayor[tope] = fin;
        }
    }
}

int* Vector::get_vector(){
    return this->vector;
}
