#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
using namespace std;

class Vector{
    private:
        int *vector;
        int cantidad;

    public:
        /* Constructor */
        Vector();
        Vector(int num);

        /* Métodos */
        void inicializar();
        void agregar_elemento(int elemento, int index);
        void metodo_seleccion();
        void metodo_qsort();
        int* get_vector();
};

#endif
