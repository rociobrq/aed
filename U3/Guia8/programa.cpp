#include <iostream>
using namespace std;
#include <cstdlib>
#include <chrono>

/* Clase */
#include "Vector.h"

void imprimir_vector(string metodo, int *vector, int num){
    cout << endl;
    cout << metodo << endl;
    for(int i = 0; i < num; i++){
        printf("v[%d]=%d ", i, vector[i]);
    }
    cout << endl;
}

bool validar_cantidad(int num){
    /* Valida si el número ingresado (cantidad de elementos a ordenar) está
    dentro de ]0, 1000000] */

    if(num > 0 && num <= 1000000){
        /* Se retorna true si cumple */
        cout << "Largo del vector: " << num << endl;
        return true;
    }
    else{
        cout << "Número ingresado debe ser positivo y menor o igual a 1000000.";
        return false;
    }
}

bool validar_ver(string &ver){
    /* Valida si el modo es "s" o "n" */
    for(int i = 0; i < ver.size(); i++){
        ver[i] = tolower(ver[i]);
    }

    if(ver == "s" || ver == "n"){
        /* Retorna true si cumple */
        cout << "Modo: " << ver << endl;
        return true;
    }
    else{
        cout << "Ingreso debe ser s: ver vectores o n: no ver vectores" << endl;
        return false;
    }
}

void llenar_vector(Vector vector, int num){
    int random;
    srand((unsigned) time(0));

    /* Generación de números aleatorios para llenar el vector*/
    for (int i = 0; i < num; i++) {
        random = (rand() % num+num) + 1;
        /* Se llama método con el número y el orden en que fue generado */
        vector.agregar_elemento(random, i);
    }
}

void ordenamiento(int *vector, Vector selec, Vector qsort, int num, string ver){
    int *vector_seleccion;
    int *vector_qsort;
    string texto;
    chrono::duration<double> duracion1, duracion2;

    /* Método selección y cálculo de su duración */
    auto t1 = chrono::high_resolution_clock::now();
    selec.metodo_seleccion();
    auto t2 = chrono::high_resolution_clock::now();
    duracion1 = (t2 - t1) * 1000;

    /* Método quicksort y cálculo de su duración */
    t1 = chrono::high_resolution_clock::now();
    qsort.metodo_qsort();
    t2 = chrono::high_resolution_clock::now();
    duracion2 = (t2 - t1) * 1000;

    /* Se obtienen vectores ordenados */
    vector_seleccion = selec.get_vector();
    vector_qsort = qsort.get_vector();

    if(ver == "s"){
        /* Imprime estado del vector generado */
        texto = "VECTOR ORIGINAL:";
        imprimir_vector(texto, vector, num);

        /* Impresión de resultados */
        cout << "\n----------------------------------" << endl;
        cout << "MÉTODO y TIEMPO EMPLEADO" << endl;
        cout << "Selección -> Tiempo: " << duracion1.count() << " ms" << endl;
        cout << "Quicksort -> Tiempo: " << duracion2.count() << " ms" << endl;
        cout << "\n----------------------------------" << endl;

        texto = "VECTOR SELECCIÓN:";
        imprimir_vector(texto, vector_seleccion, num);

        texto = "VECTOR QUICKSORT:";
        imprimir_vector(texto, vector_qsort, num);
    }

    else if(ver == "n"){
        cout << "MÉTODO y TIEMPO EMPLEADO" << endl;
        cout << "Selección -> Tiempo: " << duracion1.count() << " ms" << endl;
        cout << "Quicksort -> Tiempo: " << duracion2.count() << " ms" << endl;
    }
}

int main(int argc, char *argv[]){
    int num;
    string ver;
    bool bandera1 = false;
    bool bandera2 = false;

    /* Convierte segundo parámetro en entero (si es string, retorna 0) */
    num = atoi(argv[1]);

    /* Tercer parámetro corresponde modo de ver los elementos */
    ver = argv[2];

    /* Validación de variables */
    bandera1 = validar_cantidad(num);
    bandera2 = validar_ver(ver);

    if(bandera1 && bandera2){
        /* Generar instancia e inicializa el objeto */
        Vector obj_vector = Vector(num);
        obj_vector.inicializar();

        /* Generación de enteros aleatorios para llenar el vector */
        llenar_vector(obj_vector, num);

        /* Se obtiene vector original */
        int *vector = obj_vector.get_vector();

        /* Vector al que se le aplicará método de selección */
        Vector obj_selec = Vector(num);
        obj_selec.inicializar();

        /* Vector al que se le aplicará método quicksort */
        Vector obj_qsort = Vector(num);
        obj_qsort.inicializar();

        /* Añade elementos del vector original a ambos vectores */
        for(int i = 0; i < num; i++){
            obj_selec.agregar_elemento(vector[i], i);
            obj_qsort.agregar_elemento(vector[i], i);
        }

        ordenamiento(vector, obj_selec, obj_qsort, num, ver);

    }
    else{
        /*No se logra validar cantidad de elementos y/o modo de visualización*/
        cout << "Ha ocurrido un error." << endl;
    }

    return 0;
}
