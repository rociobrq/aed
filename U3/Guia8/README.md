# Guía 8: Vectores métodos de orden

El programa permite la generación de un vector con números aleatorios, en base a un N ingresado. Luego, se aplican dos algoritmos de ordenamiento, los que corresponden al método de selección y al método quicksort, y se calcula, además, el tiempo empleado en cada uno de ellos. Para realizar esto, al ejecutar el programa se debe ingresar un número entero que señalará el largo del vector (la cantidad de elementos a ordenar) y el modo de visualización, donde "n" imprimirá únicamente el tiempo que cada algoritmo ha tardado en ejecutar el ordenamiento y "s" imprimirá, además de lo anterior, tanto el vector original (desordenado) como los vectores ordenados por selección y quicksort.  Al iniciar el programa y haber recibido ambos datos, se realiza una validación de ellos, donde se revisa que el número sea mayor que 0 y menor que 1000000 y que la letra ingresada corresponda a una de las dos alternativas: "n" o "s". Si ambos input están correctos, entonces se ejecuta una instancia a la clase Vector, la que permite crear un vector con N espacios al usar el método Vector:: inicializar(). Luego, se generan números aleatorios que se agregan al vector usando el método Vector::agregar_elemento(int elemento, int index). Para trabajar con el vector en el programa principal, se recupera el original con Vector::get_vector(). Seguidamente, se generan dos instancias nuevas a la clase Vector para generar los vectores que se van a ordenar con ambos algoritmos, siendo estos copias del vector original. Finalmente, se emplean los algoritmos de ordenamiento mencionados anteriormente con Vector::metodo_seleccion y Vector::metodo_qsort, donde:

* Selección: Corresponde a un método que avanza desde el primer índice hasta el último, comparando en cada oportunidad al elemento actual con el resto. Si se encuentran números menores que el actual, entonces se escoge el más bajo de ellos y aquel se intercambia de posición con el actual.
* Quicksort: Corresponde a un método que compara un elemento primeramente recorriendo el vector desde derecha a izquierda y luego de izquierda a derecha.

Finalmente, para ambos métodos se mide el tiempo usando la librería *chrono* y la función chrono::high resolution clock::now().

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Vector.cpp

Luego, para ejecutar el programa se debe utilizar:
* ./programa número modo

Donde:
* número: Cantidad de elementos del vector
* modo: s (ver elementos del vector original y de los vectores ordenados) o n (no ver elementos)

Ejemplo 1: 20 elementos mostrando contenido de vectores
* ./programa 20 s

Ejemplo 2: 20 elementos sin mostrar contenido de vectores
* ./programa 20 n

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U2/Guia8

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* https://www.geeksforgeeks.org/ways-copy-vector-c/
* https://www.youtube.com/watch?v=oEx5vGNFrLk&t=211s
* https://www.youtube.com/watch?v=hlC1H8U0WXk&t=381s
* https://stackoverflow.com/questions/4736485/srandtime0-and-random-number-generation
