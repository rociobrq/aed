#include <iostream>
using namespace std;
#include <vector>
#include <algorithm>

#include "Tabla.h"
#include "Nodo.h"

int menu(){
    int op;

    do{
        cout << "\n[1] Insertar" << endl;
        cout << "[2] Buscar" << endl;
        cout << "[3] Salir" << endl;
        cout << "\nOpción: ";
        cin >> op;

        /* Si opción recibida no es válida, se pide nuevamente */
        if(cin.fail()){
            cout << "Ha ocurrido un error. " << endl;
            cin.clear();
            cin.ignore();
        }
    } while(op < 1 || op > 3);

    return op;
}

bool validar_metodo(string &metodo){
    /* Evalúa si el método ingresado corresponde a L, C, D o E */
    for(int i = 0; i < metodo.size(); i++){
        /* Se transforma el string a mayúscula */
        metodo[i] = toupper(metodo[i]);
    }

    /* Si el método coincide, se retorna true */
    if(metodo == "L" || metodo == "C" || metodo == "D" || metodo == "E"){
        /* Retorna true si cumple */
        cout << "Método resolución de colisiones: " << metodo << endl;
        return true;
    }
    else{
        cout << metodo << " no coincide con un método de resolución." << endl;
        return false;
    }
}

int ingreso(vector<int> claves){
    int cl;

    while(true){
        cout << "Ingresar elemento: ";
        cin >> cl;
        /* Si ingreso no es int, es menor o igual a cero o ya está dentro de
        las claves ingresadas, se pide nuevamente */
        if(cin.fail() || cl <= 0 || count(claves.begin(), claves.end(), cl)){
            cin.clear();
            cin.ignore();
        }
        else{
            break;
        }
    }

    /* Se retorna clave a ingresar */
    return cl;
}

int ingreso_busqueda(){
    int cl;

    while(true){
        cout << "Ingresar elemento: ";
        cin >> cl;
        /* Si ingreso no es int, es menor o igual a cero se pide nuevamente */
        if(cin.fail() || cl <= 0){
            cin.clear();
            cin.ignore();
        }
        else{
            break;
        }
    }

    /* Se retorna clave a buscar*/
    return cl;
}


int main(int argc, char *argv[]){
    string metodo;
    int clave, opcion;
    vector<int> claves;
    Tabla tabla_hash;
    Nodo *nodo;
    bool band, lleno, ingresado;

    system("clear");

    /* Recupera método para solucionar colisiones */
    metodo = argv[1];
    band = validar_metodo(metodo);

    /* Si se valida el método, se genera la tabla hash */
    if(band){
        /* Se crea objeto tabla */
        tabla_hash = Tabla(20);

        /* Método de encademiento inicializa un array con nodos */
        if(metodo == "E"){
            tabla_hash.inicializar_listas();
        }
        else{
            /* Los otros métodos inicializan un array con enteros */
            tabla_hash.inicializar_array();
            lleno = false;
        }

        opcion = menu();

        while (opcion){
            switch(opcion){
                case 1:
                    /* Variable cambia a true ante ingreso de clave */
                    ingresado = false;

                    if(metodo == "E"){
                        /* Se pide clave a ingresar */
                        clave = ingreso(claves);

                        /* Se añade clave al nodo */
                        nodo = new Nodo;
                        nodo->info = clave;
                        nodo->sig = NULL;

                        /* Llamada a método para insertar con lista enlazada */
                        tabla_hash.insertar_encadenamiento(nodo, ingresado);
                    }
                    else{
                        /* Se llama a método para saber si el arreglo tiene
                        espacio. Método de prueba lineal, cuadrática y doble
                        hash cuentan con posiciones limitadas */
                        tabla_hash.arreglo_lleno(lleno);

                        if(lleno == false){
                            /* Se pide clave */
                            clave = ingreso(claves);

                            /* Identificar método para insertar */
                            if(metodo == "L"){
                                tabla_hash.insertar_plineal(clave, ingresado);
                            }
                            else if(metodo == "C"){
                                tabla_hash.insertar_pcuadratica(clave, ingresado);
                            }
                            else if(metodo == "D"){
                                tabla_hash.insertar_doble_hash(clave, ingresado);
                            }
                        }
                        else{
                            cout << "Array lleno. Imposible ingresar." << endl;
                        }
                    }

                    /* Tras ingreso exitoso, se añade clave a un vector */
                    if(ingresado == true){
                        claves.push_back(clave);
                    }

                    break;

                case 2:
                    /* Se pide clave a buscar */
                    clave = ingreso_busqueda();

                    /* Se llaman a los métodos correspondientes pasando la
                    clave como parámetro */
                    if(metodo == "E"){
                        tabla_hash.buscar_encadenamiento(clave);
                    }
                    else{
                        if(metodo == "L"){
                            tabla_hash.buscar_plineal(clave);

                        }
                        else if(metodo == "C"){
                            tabla_hash.buscar_pcuadratica(clave);
                        }
                        else if(metodo == "D"){
                            tabla_hash.buscar_doble_hash(clave);
                        }
                    }

                    break;

                case 3:
                    /* Termina ejecución del programa */
                    cout << "Saliendo..." << endl;
                    exit(0);
            }
            opcion = menu();
        }
    }
    return 0;
}
