#ifndef TABLA_H
#define TABLA_H

#include <iostream>
using namespace std;

#include <string>

// #include "Lista.h"
#include "Nodo.h"

class Tabla{
    private:
        int n;
        int array[20];
        Nodo *array_listas[20];

    public:
        /* Constructor */
        Tabla();
        Tabla(int largo);

        /* Métodos */
        void inicializar_array();
        void inicializar_listas();
        int funcion_hash(int clave);
        int funcion_hash2(int direccion);
        void insertar_plineal(int clave, bool &ingresado);
        void insertar_pcuadratica(int clave, bool &ingresado);
        void insertar_doble_hash(int clave, bool &ingresado);
        void arreglo_lleno(bool &band);
        void insertar_encadenamiento(Nodo *nodo, bool &ingresado);
        void buscar_plineal(int clave);
        void buscar_pcuadratica(int clave);
        void buscar_doble_hash(int clave);
        void buscar_encadenamiento(int clave);
        void imprimir_arreglo();
        void imprimir_listas_enlazadas();
};

#endif
