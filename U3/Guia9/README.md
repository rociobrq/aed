# Guía 9: Métodos de búsqueda - tablas Hash

El programa permite seleccionar un método entre cuatro disponibles para tratar las colisiones que se puedan producir al ingresar elementos a un arreglo de 20 casillas. Estos cuatro métodos corresponden a: prueba lineal (L), prueba cuadrática (C), doble dirección hash (D) y encadenamiento (E). Cuando el programa identifica el método, se genera el objeto tabla_hash que corresponde a una instancia de la clase "Tabla.h", la que permite generar dos arreglos dependiendo de la necesidad; si se trabaja con L, C o D, se genera un arreglo de enteros con el método Tabla::inicializar_array(); mientras tanto, si se trabaja con E, se genera un arreglo de nodos con Tabla::inicializar_listas(). Luego, Se pueden identificar tres opciones:

1. Inserción. Primeramente, para ingresar se debe verificar que el arreglo no esté lleno (válido para métodos L/C/D). Luego, se pide la clave a ingresar al arreglo, clave que necesita ser un número entero mayor a 0 y además no repetirse.  Para los tres primeros (L/C/D) se trabaja con enteros, por lo que al validar la clave se llama al método correspondiente. Para el método E, se trabaja con nodos y al validar la clave se permite la generación del nuevo nodo. En cada método se llama a una función hash principal Tabla:funcion_hash(int clave), la que se define por *clave mod 20* y retorna el índice correspondiente en el arreglo. Luego, se tiene:

* L: Tabla::insertar_plineal(int clave, bool &ingresado). Si la dirección ingresada no contiene elementos, se añade. Si está ocupada, se produce una colisión que se soluciona revisando desde aquella posición hacia las siguientes, incrementando el índice en una unidad. Si se llega a la última posición sin haber añadido el elemento, se reinicia en 0 y se vuelve a revisar.
* C: Tabla::insertar_plineal(int clave, bool &ingresado). Si la dirección ingresada no contiene elementos, se añade la clave. Si la posición ya contiene un elemento, entonces se soluciona con una revisión de casillas que responde a la operación *indice + i^2*. Si se genera un valor que supera N - 1 (19), se reinicia y se vuelve a revisar desde la casilla inicial. Este método puede generar bucles donde, aun cuando hay posiciones, no se puede ingresar el elemento porque no se identifica esa casilla con la operación anteriormente mencionada. Si es posible encontrar una casilla vacía, entonces se añade el elemento.
* D: Tabla::insertar_plineal(int clave, bool &ingresado). Si la dirección ingresada no permite añadir el elemento nuevo, se llama una segunda función hash que responde al método Tabla::funcion_hash2(int direccion), la que retorna un valor *direccion + 1) mod 20*.
* E: Tabla::insertar_encadenamiento(int clave, bool &ingresado). A diferencia de las anteriores, este método no tiene un límite, ya que si se produce una colisión, se genera una lista enlazada donde se van añadiendo los nodos cuya dirección, dada por la función hash, es la misma.

2. Búsqueda. La búsqueda pide la clave y, tras su validación, se ejecutan los métodos correspondientes en base a la letra ingresada. Los siguientes algoritmos son similares a los de inserción, únicamente difiriendo en que no buscan casillas vacías, sino que identifican la clave y entregan su posición:

* L: Tabla::buscar_plineal(int clave).
* C: Tabla::buscar_pcuadratica(int clave).
* D: Tabla::buscar_doble_hash(int clave).
* E: Tabla::buscar_encadenamiento(int clave).

3. Salida: Termina ejecución del programa.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ hash.cpp Tabla.cpp

Luego, para ejecutar el programa se debe utilizar:
* ./hash {L|C|D|E}

Donde:
* L soluciona colisiones con reasignación - prueba lineal.
* C soluciona colisiones con reasignación - prueba cuadrática.
* D soluciona colisiones con reasignación - prueba doble dirección hash.
* E soluciona colisiones con reasignación - Encadenamiento.

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U3/Guia9

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* https://www.geeksforgeeks.org/quadratic-probing-in-hashing/
* https://www.w3schools.com/cpp/cpp_strings_concat.asp
* http://www.cplusplus.com/reference/string/to_string/
* https://www.geeksforgeeks.org/initialize-a-vector-in-cpp-different-ways/
* https://www.tutorialspoint.com/cplusplus-program-to-implement-hash-tables
