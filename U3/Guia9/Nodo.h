#ifndef NODO_H
#define NODO_H

/* Estructura del nodo */
typedef struct _Nodo {
    int info;
    struct _Nodo *sig;
} Nodo;

#endif
