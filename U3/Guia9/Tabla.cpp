#include <iostream>
using namespace std;

/* Definición de clase */
#include "Tabla.h"

/* Constructores */
Tabla::Tabla(){
    int n;
    /* Array para método de prueba lineal, cuadrática y doble hash */
    int array[20];
    /* Array de listas enlazadas para método de enlazamiento */
    Nodo *array_listas[20];
}

Tabla::Tabla(int largo){
    this->n = largo;
    this->array[20] = {0};
    this->array_listas[20] = NULL;
}

/* Métodos */
void Tabla::inicializar_array(){
    /* Todas las posiciones del array se inicializan en null*/
    for(int i = 0; i < n; i++){
        array[i] = 0;
    }
}

void Tabla::inicializar_listas(){
    /* Posiciones vacías de array de listas enlazadas */
    for(int i = 0; i < n; i++){
        array_listas[i] = NULL;
    }
}

int Tabla::funcion_hash(int clave){
    /* A partir de la clave se calcula el índice en el arreglo */
    int indice = (clave % 20);
    return indice;
}

int Tabla::funcion_hash2(int direccion){
    /* Función hash calcula índice en el arreglo a partir de índice previo */
    int indice = ((direccion + 1) % 20);
    return indice;
}

void Tabla::arreglo_lleno(bool &band){
    int espacios = 0;

    /* Revisa todas las posciones del arreglo */
    for(int i = 0; i < n; i++){
        if(array[i] == 0){
            espacios++;
        }
    }

    /* No hay ninguna casilla libre */
    if(espacios == 0){
        band = true;
    }
}

void Tabla::insertar_plineal(int clave, bool &ingresado){
    int indice = funcion_hash(clave);
    string colision;
    int aux;

    cout << "\nIngresando..." << endl;

    /* Si casilla está vacía, se agrega el elemento y se imprime tabla */
    if(array[indice] == 0){
        array[indice] = clave;
        ingresado = true;
        imprimir_arreglo();
    }
    else{
        /* Al producirse colisión, se guarda índice */
        printf("Colisión en a[%d]. Método: R - Prueba Lineal", indice);
        colision = to_string(indice);

        /* Prueba lineal incrementa en una unidad */
        aux = indice + 1;

        /* Se avanza cada una posición a través del arreglo */
        while(array[aux] != 0){
            colision = colision + " -> " + to_string(aux);
            /* Si el índice es menor que el máximo se incrementa casilla*/
            if(aux < n - 1){
                aux++;
            }
            /* Al superar el máximo se regresa a la primera posición */
            else{
                aux = 0;
            }
        }

        /* Desplazamiento final */
        colision = colision + " -> " + to_string(aux);
        cout << "\nDesplazamiento: " << colision << endl;

        /* Se añade el elemento*/
        array[aux]  = clave;
        ingresado = true;
        imprimir_arreglo();
    }
}

void Tabla::insertar_pcuadratica(int clave, bool &ingresado){
    int indice = funcion_hash(clave);
    int i, aux, ronda;
    string colision;

    cout << "\nIngresando..." << endl;

    /* Casilla vacía permite añadir elemento nuevo */
    if(array[indice] == 0){
        array[indice] = clave;
        ingresado = true;
        imprimir_arreglo();
    }
    else{
        /* Colisión */
        printf("Colisión en a[%d]. Método: R - Prueba cuadrática ", indice);
        colision = to_string(indice);

        /* Se recorre array desde índice resultante mientras las casillas
        contengan un elemento */

        i = 1;
        aux = indice + (i * i);
        ronda = 0;

        while((array[aux - 1] != 0)){
            i = i + 1;
            /* Recorre arreglo indice + i^2*/
            aux = (indice + (i * i));

            if(aux > n){
                i = 0;
                aux = 1;
                indice = 1;
            }

            /* Registra casilla */
            colision = colision + " -> " + to_string(aux - 1);

            /* Incrementa ronda */
            ronda++;

            if(ronda > n){
                break;
            }
        }

        if(aux > 0){
            aux = aux - 1;
        }

        /* Desplazamiento final */
        cout << "\nDesplazamiento: " << colision << endl;

        if(array[aux] == 0){
            /* Se añade el elemento al arreglo */
            array[aux]  = clave;
            ingresado = true;
            imprimir_arreglo();

        }
        else{
            cout << "\nNo se ha podido añadir elemento.";
        }
    }
}

void Tabla::insertar_doble_hash(int clave, bool &ingresado){
    int indice = funcion_hash(clave);
    string colision;

    cout << "\nIngresando..." << endl;

    /* Si índice está vacío, se añade clave */
    if(array[indice] == 0){
        array[indice] = clave;
        ingresado = true;
        imprimir_arreglo();
    }
    else{
        printf("Colisión en a[%d]. Método: R - Doble dirección hash\n", indice);
        colision = to_string(indice);

        /* Se recorre array utilizando otra función hash */
        int aux = funcion_hash2(indice);

        while((array[aux] != 0) && (aux <= n - 1) && (aux != indice)){
            colision = colision + " -> " + to_string(aux);
            aux  = funcion_hash2(aux);
        }

        /* Desplazamiento final */
        colision = colision + " -> " + to_string(aux);
        cout << "\nDesplazamiento: " << colision << endl;

        indice = aux;

        /* Se añade clave */
        if(array[indice] == 0){
            array[indice] = clave;
            ingresado = true;
            imprimir_arreglo();
        }
        else{
            cout << "No se pudo agregar el elemento al arreglo." << endl;
        }
    }
}

void Tabla::insertar_encadenamiento(Nodo *nodo, bool &ingresado){
    /* Obtención de la casilla */
    int i = 0;
    int indice = funcion_hash(nodo->info);
    string colision;

    /* Casilla no contiene un nodo */
    if(array_listas[indice] == NULL){
        array_listas[indice] = nodo;
        ingresado = true;
    }
    else{
        /* Colisión */
        printf("Colisión en a[%d]. Método: R - Encadenamiento", indice);
        colision = to_string(indice) + "." + to_string(i);

        Nodo* tmp = array_listas[indice];

        /* Recorre lista enlazada hasta el final */
        while(tmp->sig != NULL){
            i = i + 1;
            colision = colision + "->" + to_string(indice) + "." + to_string(i);
            tmp = tmp->sig;
        }

        /* Desplazamiento final */
        i = i + 1;
        colision = colision + "->" + to_string(indice) + "." + to_string(i);
        cout << "\nDesplazamiento: " << colision << endl;

        /* Nodo nuevo se añade al final */
        ingresado = true;
        tmp->sig = nodo;
    }

    imprimir_listas_enlazadas();
}

void Tabla::buscar_plineal(int clave){
    int indice = funcion_hash(clave);
    int aux;
    string colision;

    /* Casilla posee un elemento y la clave coincide con búsqueda */
    if((array[indice] != 0) && (array[indice] == clave)){
        printf("array[%d] ", indice);
        cout << "Información encontrada en la posición: " << indice << endl;
    }
    else{
        /* Al producirse colisión, se guarda índice */
        printf("Colisión en a[%d]. Método: R - Prueba Lineal", indice);
        colision = to_string(indice);

        /* Se recorre array desde índice resultante mientras no exista
        coincidencia y casilla no esté vacía */
        aux = indice + 1;

        while((aux <= n)&& (array[aux] != 0)&& (array[aux] != clave) &&
            (aux != indice)){
            /* Registra desplazamiento */
            colision = colision + " -> " + to_string(aux);
            aux = aux + 1;

            if(aux == n){
                aux = 0;
            }
        }

        /* Desplazamiento final */
        colision = colision + " -> " + to_string(aux);
        cout << "\nDesplazamiento: " << colision << endl;

        /* Al llegar a casilla vacía o al mismo índice, no hay resultado */
        if((array[aux] == 0) || (aux == indice)){
            cout << "La información no se encuentra en el arreglo." << endl;
        }
        else{
            cout << "Información encontrada en la posición: " << aux << endl;
        }
    }
}

void Tabla::buscar_pcuadratica(int clave){
    int indice = funcion_hash(clave);
    int i;
    int aux;
    int ronda;
    string colision;

    /* Casilla posee un elemento y la clave coincide con búsqueda */
    if((array[indice] != 0) && (array[indice] == clave)){
        cout << "Información en la posición: " << indice;
    }
    else{
        printf("Colisión en a[%d]. Método: R - Prueba cuadrática ", indice);
        colision = to_string(indice);

        /* Se recorre array desde índice resultante mientras no exista
        coincidencia y casilla contenga un elemento */
        i = 1;
        aux = indice + (i * i);
        ronda = 0;

        while((array[aux - 1] != 0) && (array[aux - 1] != clave)){
            i = i + 1;
            /* Recorre casillas en base a cuadrados */
            aux = indice + (i * i);

            if(aux > n){
                i = 0;
                aux = 1;
                indice = 1;
            }
            /* Registra dirección recorrida */
            colision = colision + " -> " + to_string(aux - 1);

            /* Incrementa cantidad de ciclos */
            ronda++;

            /* Si las rondas superan a n, se rompe el ciclo, ya que se revisan
            las mismas posiciones que contienen casillas ocupadas */
            if(ronda > n){
                break;
            }
        }

        if(aux > 0){
            aux = aux - 1;
        }

        /* Desplazamiento final */
        cout << "\nDesplazamiento: " << colision << endl;

        if(array[aux] == 0 || array[aux] != clave){
            cout << "La información no se encuentra en el arreglo." << endl;
        }
        else{
            cout << "Información en la posición: " << aux;
        }
    }
}

void Tabla::buscar_doble_hash(int clave){
    int indice = funcion_hash(clave);
    string colision;

    /* Elemento es encontrado */
    if((array[indice] != 0) && (array[indice] == clave)){
        cout << "Información en la posición: " << indice;
    }
    else{
        printf("Colisión en a[%d]. Método: R - Doble dirección hash\n", indice);
        colision = to_string(indice);

        /* Se recorre array utilizando otra función hash */
        int aux = funcion_hash2(indice);

        while((array[aux] != 0) && (array[aux] != clave) &&
            (aux <= n - 1) && (aux != indice)){
            colision = colision + " -> " + to_string(aux);
            aux  = funcion_hash2(aux);
        }

        /* Desplazamiento final */
        colision = colision + " -> " + to_string(aux);
        cout << "\nDesplazamiento: " << colision << endl;

        /* Si casilla está vacía o posee un elemento cuya clave no coincide,
        indica búsqueda sin resultados */
        if(array[aux] == 0 || (array[aux] != clave)){
            cout << "La información no se encuentra en el arreglo." << endl;
        }
        else{
            cout << "Información en la posición: " << aux;
        }
    }
}

void Tabla::buscar_encadenamiento(int clave){
    int i;
    Nodo* aux;
    int indice = funcion_hash(clave);
    string colision;

    /* Casilla contiene un nodo */
    if(array[indice] != 0){
        /* Si coincide con clave se determina posición */
        if(array_listas[indice]->info == clave){
            cout << "Información en la posicion: " << indice << endl;
        }
        else{
            printf("Colisión en a[%d]. Método: R - Encadenamiento", indice);
            colision = to_string(indice) + "." + to_string(i);

            /* Al no coincidir con clave, se avanza en la lista enlazada */
            aux = array_listas[indice]->sig;

            while((aux != NULL) && aux->info != clave){
                i = i + 1;
                colision = colision + "->" + to_string(indice) + "." + to_string(i);
                aux = aux->sig;
            }

            /* Desplazamiento final */
            i = i + 1;
            colision = colision + "->" + to_string(indice) + "." + to_string(i);
            cout << "\nDesplazamiento: " << colision << endl;

            /* Se llega al final de la lista */
            if(aux == NULL){
                cout << "La información no se encuentra en la lista," << endl;
            }
            /* Se encuentra el nodo */
            else{
                cout << "La información está en la lista." << endl;
            }
        }
    }
    /* Casilla no contiene un nodo */
    else{
        cout << "La información no se encuentra en la lista." << endl;
    }
}

void Tabla::imprimir_arreglo(){
    printf("\n");
    for(int i = 0; i < n; i++){
        if(array[i] == 0){
            printf("array[%d] ", i);
            cout << "NULL" << endl;
        }
        else{
            printf("array[%d] ", i);
            cout << array[i] << endl;
        }
    }

    printf("\n");
}

void Tabla::imprimir_listas_enlazadas(){
    printf("\n");
    for(int i = 0; i < n; i++){
        printf("array[%d] ", i);
        // Se apunta al nodo en la raíz.
        Nodo *tmp = array_listas[i];

        if(tmp == NULL){
            cout << " -> NULL" << endl;
        }
        else{
            // Mientras existan nodos, se continúa imprimiendo.
            while(tmp != NULL){
                cout << " -> " << tmp->info;
                // Apunta al nodo sig. Si es el último, estará apuntando a NULL.
                tmp = tmp->sig;
            }
            printf("\n");
        }
    }
    printf("\n");
}
