#include <iostream>
using namespace std;

// Clases.
#include "Principal.h"

void menu(int &opc){
    while(true){
        cout << "\n¿Desea ingresar otro nodo a la lista? SÍ: 1, NO: 0" << endl;
        cout << "Opción: ";
        if(cin >> opc){
            break;
        }
        else{
            cout << "Se ha producido un error. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }
}

void ingreso_num(int &numero){
    while(true){
        cout << "\nIngrese un número: ";
        if(cin >> numero){
            // Número entero, se rompe el ciclo.
            break;
        }
        else{
            cout << "El valor es inválido. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }
}

void ingreso_lista(Lista *lista){
    int numero = 0;
    int opc = 0;

    // Se ingresa primer nodo de la lista.
    ingreso_num(numero);
    lista->crear_nodo(numero);
    lista->imprimir();

    while(true){
        // Se siguen añadiendo nodos hasta que opción sea 0.
        menu(opc);

        if(opc == 1){
            ingreso_num(numero);
            lista->crear_nodo(numero);
            lista->imprimir();
        }
        else if(opc == 0){
            // Rompe el ciclo.
            break;
        }

        else{
            cout << "No válido." << endl;
        }
    }
}

int main(){
    // Creación de primera lista.
    Principal p1 = Principal();
    Lista *lista1 = p1.get_lista();

    // Ingreso de nodos lista 1.
    cout << "\nIngreso a lista 1" << endl;
    ingreso_lista(lista1);

    // Creación de segunda lista.
    Principal p2 = Principal();
    Lista *lista2 = p2.get_lista();

    // Ingreso de nodos lista 2.
    cout << "\nIngreso a lista 2" << endl;
    ingreso_lista(lista2);

    // Generación de tercera lista.
    Principal p3 = Principal();
    Lista *lista3 = p3.get_lista();

    // Se obtiene la tercera lista.
    lista3->mezclar_listas(lista1, lista2);

    // Impresión de cada lista.
    cout << "\nLista 1: ";
    lista1->imprimir();
    cout << "Lista 2: ";
    lista2->imprimir();
    cout << "Lista 3: ";
    lista3->imprimir();

    return 0;
}
