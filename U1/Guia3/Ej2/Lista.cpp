#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){}

void Lista::crear_nodo(int num){
    // Se genera el nuevo nodo.
    Nodo *tmp = new Nodo;

    // Su valor es el número y apunta a NULL.
    tmp->num = num;
    tmp->sig = NULL;

    // Evalúa si corresponde al primer nodo.
    if(this->raiz == NULL){
        // Se ingresa primer nodo y queda como último nodo.
        this->raiz = tmp;
        this->final = this->raiz;
    }

    // Evalúa si el número es menor que el resto (se colocará primero).
    else if(tmp->num <= this->raiz->num){
        // Se guarda la actual raiz en variable auxiliar.
        Nodo *aux = this->raiz;
        // Nuevo nodo apunta a la actual raiz y actualiza raiz con nuevo nodo.
        tmp->sig = aux;
        this->raiz = tmp;
    }

    // Evalúa si el número es mayor que el último.
    else if(tmp->num > this->final->num){
        // Apunta el último nodo al siguiente  y se actualiza último nodo.
        this->final->sig = tmp;
        this->final = tmp;
    }

    // Se busca la posición donde debe ir el nuevo nodo.
    else{
        // Se realiza la búsqueda desde el primer nodo como referencia.
        Nodo *ref = this->raiz;
        Nodo *anterior;

        // Se encuentra un nodo mayor/igual para colocar el nuevo nodo antes.
        while(tmp->num > ref->num){
            // Se guarda nodo previo.
            anterior = ref;
            // Se apunta al nodo siguiente.
            ref = ref->sig;
        }

        // Cuando se rompe el ciclo, se ha encontrado un nodo con un número
        // mayor/igual, entonces el nodo previo apuntará al nuevo nodo.
        anterior->sig = tmp;
        // El nuevo nodo apuntará al nodo de referencia.
        tmp->sig = ref;

    }
}

void Lista::mezclar_listas(Lista *lista1, Lista *lista2){
    Nodo *nodo1 = lista1->raiz;
    Nodo *nodo2 = lista2->raiz;
    Nodo *nuevo = new Nodo;
    Nodo *aux;
    int band = 0;

    // Hallar raiz.
    // Se compara el primer número de cada lista.
    if(nodo1->num < nodo2->num){
        // Lista 1 con número menor. Nodo nuevo guarda su número.
        nuevo->num = nodo1->num;
        nuevo->sig = NULL;
        nodo1 = nodo1->sig;
    }
    else{
        // Lista 2 con número menor. Nodo nuevo guarda su número.
        nuevo->num = nodo2->num;
        nuevo->sig = NULL;
        nodo2 = nodo2->sig;
    }

    // Se guarda la raíz y el nodo final.
    this->raiz = nuevo;
    this->final = nuevo;
    // Variable aux empieza desde la raíz.
    aux = this->raiz;

    while(true){
        // Se crea nuevo nodo para añadir a la lista 3.
        Nodo *nuevo = new Nodo;

        if(band == 0){
            // Para obtener número siguiente, se comparan nodos de las listas.
            // Número menor en lista 1.
            if(nodo1->num <= nodo2->num){
                // Se añade número al nodo nuevo.
                nuevo->num = nodo1->num;
                if(nodo1->sig != NULL){
                    // Si no se ha llegado al final de lista 1, se sigue
                    // recorriendo lista 1.
                    nodo1 = nodo1->sig;
                }
                else{
                    // Se llega al final de la lista 1.
                    band = 1;
                }
            }
            // Número menor en lista 2.
            else{
                // Se añade el número al nodo nuevo.
                nuevo->num = nodo2->num;
                if(nodo2->sig != NULL){
                    // Si no se ha llegado al final de la lista 2, se sigue
                    // recorriendo lista 2.
                    nodo2 = nodo2->sig;
                }
                else{
                    // Se llega al final de la lista 2.
                    band = 2;
                }
            }

            // El nodo nuevo apunta a null y el aux apunta al nodo nuevo.
            nuevo->sig = NULL;
            aux->sig = nuevo;
            // Ahora se continúa enlazando desde el nodo nuevo.
            aux = nuevo;
        }

        else if(band == 1){
            // Si se llegó al final de la lista 1, entonces el nuevo nodo
            // toma el número del nodo actual de la lista 2.
            nuevo->num = nodo2->num;
            nuevo->sig = NULL;
            // Nodo previo apunta a nuevo nodo.
            aux->sig = nuevo;

            if(nodo2->sig == NULL){
                // Si se llega al final de la lista 2, se actualiza el final.
                this->final = nuevo;
                break;
            }
            else{
                // Si no se ha llegado al final de la lista 2, se sigue
                // avanzando en esta.
                nodo2 = nodo2->sig;
            }
        }
        else if(band == 2){
            // Si se llega al final de lista 2, el nuevo nodo toma el número
            // del nodo actual de la lista 1.
            nuevo->num = nodo1->num;
            nuevo->sig = NULL;
            // Nodo previo apunta a nodo nuevo.
            aux->sig = nuevo;

            if(nodo1->sig == NULL){
                // Si se llega al final de lista 1, el nodo nuevo es el último
                // ingreso de la lista 3.
                this->final = nuevo;
                break;
            }
            else{
                // Si no se ha terminado de recorrer lista 1, se avanza.
                nodo1 = nodo1->sig;
            }
        }
    }
}

void Lista::imprimir(){
    // Se apunta al nodo en la raíz.
    Nodo *tmp = this->raiz;

    // Mientras existan nodos, se continúa imprimiendo.
    while(tmp != NULL){
        cout << tmp->num << " | ";
        // Apunta al nodo siguiente. Si es el último, estará apuntando a NULL.
        tmp = tmp->sig;
    }

    cout << "\n";
}
