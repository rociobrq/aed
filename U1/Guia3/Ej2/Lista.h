#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

// Estructura del nodo.
typedef struct _Nodo {
    int num;
    struct _Nodo *sig;
} Nodo;


class Lista{
    private:
        Nodo *raiz = NULL;
        Nodo *final = NULL;

    public:
        // Constructor.
        Lista();

        // Métodos.
        // Crea un nodo recibiendo un número entero.
        void crear_nodo(int numero);
        // Mezcla las listas manteniendo el orden.
        void mezclar_listas(Lista *lista1, Lista *lista2);
        // Imprime el estado de la lista.
        void imprimir();
};
#endif
