# Ejercicio 2

El objetivo de este programa es generar dos listas enlazadas ordenadas, para finalmente generar una tercera lista que resulte del entrelazamiento de las dos listas anteriores y mantenga así el orden. La lista tiene como atributos el nodo raíz y el nodo final de la misma. En base a esto, el primer número ingresado será tanto el nodo raíz como el nodo final. Seguidamente, el ingreso de números continuará siempre que el usuario seleccione la opción 1, enseñando en cada oportunidad el estado actual de la lista. Básicamente, en cada ingreso se evaluarán los siguientes escenarios:

1. Si es menor/igual al número del nodo raíz, entonces se guarda la actual raíz en una variable auxiliar, se actualiza la raíz con el nuevo nodo y este apunta ahora al auxiliar.
2. Si es mayor al número del nodo final, este último apuntará al nuevo nodo y se actualizará el nodo final.
3. Si no cumple con ninguna de las anteriores, entonces se procede a recorrer la lista desde el nodo raíz hasta encontrar un nodo que contenga un número mayor o igual y sirva como referencia para la ubicación. Cuando se encuentra este nodo, el nodo anterior estará apuntando al nuevo nodo y este, a su vez, apuntará al de referencia.

Luego de ingresar números a ambas listas, se genera una tercera lista y se llama al método mezclar_listas() de la clase Lista. Este método se encarga, primeramente, se encontrar la raíz de la nueva lista; para esto, revisa qué lista tiene el número menor en su primer nodo y, una vez determinado, crea un nuevo nodo y lo guarda en una variable auxiliar. Luego, se efectúa un ciclo en el que se va recorriendo la lista 1 con "nodo1" y la lista 2 con "nodo2". En este proceso, se va evaluando cuál de las listas tiene el siguiente número más pequeño; una vez es encontrado, se genera un nuevo nodo que apunta a NULL y el nodo en la variable auxiliar apunta a este nuevo nodo. En este punto, se consulta si se ha llegado al final de la lista en la que se ha hallado el número: si no se ha llegado al final, se avanza al siguiente nodo de esa lista; en caso contrario, se siguen añadiendo los nodos de la otra lista hasta llegar al final.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Principal.cpp Lista.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./ej2

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.
