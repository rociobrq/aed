#ifndef PRINCIPAL_H
#define PRINCIPAL_H

#include <iostream>
using namespace std;
#include "Lista.h"

class Principal{
    private:
        Lista *lista = NULL;

    public:
        // Constructor.
        Principal();

        // Métodos.
        Lista *get_lista();
};
#endif
