#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){}

void Lista::crear_nodo(int num){
    // Se genera el nuevo nodo.
    Nodo *tmp = new Nodo;

    // Su valor es el número y apunta a NULL.
    tmp->num = num;
    tmp->sig = NULL;

    // Evalúa si corresponde al primer nodo.
    if(this->raiz == NULL){
        // Se ingresa primer nodo y queda como último nodo.
        this->raiz = tmp;
        this->final = this->raiz;
    }

    // Evalúa si el número es menor que el resto (se colocará primero).
    else if(tmp->num <= this->raiz->num){
        // Se guarda la actual raiz en variable auxiliar.
        Nodo *aux = this->raiz;
        // Nuevo nodo apunta a la actual raiz y actualiza raiz con nuevo nodo.
        tmp->sig = aux;
        this->raiz = tmp;
    }

    // Evalúa si el número es mayor que el último.
    else if(tmp->num > this->final->num){
        // Apunta el último nodo al siguiente  y se actualiza último nodo.
        this->final->sig = tmp;
        this->final = tmp;
    }

    // Se busca la posición donde debe ir el nuevo nodo.
    else{
        // Se realiza la búsqueda desde el primer nodo como referencia.
        Nodo *ref = this->raiz;
        Nodo *anterior;

        // Se encuentra un nodo mayor/igual para colocar el nuevo nodo antes.
        while(tmp->num > ref->num){
            // Se guarda nodo previo.
            anterior = ref;
            // Se apunta al nodo siguiente.
            ref = ref->sig;
        }

        // Cuando se rompe el ciclo, se ha encontrado un nodo con un número
        // mayor/igual, entonces el nodo previo apuntará al nuevo nodo.
        anterior->sig = tmp;
        // El nuevo nodo apuntará al nodo de referencia.
        tmp->sig = ref;
    }
}

void Lista::imprimir(){
    // Se apunta al nodo en la raíz.
    Nodo *tmp = this->raiz;

    // Mientras existan nodos, se continúa imprimiendo.
    while(tmp != NULL){
        cout << tmp->num << " | ";
        // Apunta al nodo siguiente. Si es el último, estará apuntando a NULL.
        tmp = tmp->sig;
    }

    cout << "\n";
}
