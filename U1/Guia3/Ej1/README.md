# Ejercicio 1

La finalidad del programa es permitir el ingreso de números enteros para generar una lista enlazada simple ordenada. Primeramente se pide un número inicial para la lista, luego se procede a generar el primer nodo que llevará como valor aquel número y estará apuntando a NULL. Este primer nodo será el nodo raíz de la lista y también el nodo final de la misma. Seguidamente, el ingreso de números continuará siempre que el usuario seleccione la opción 1, enseñando en cada oportunidad el estado actual de la lista. Básicamente, en cada ingreso se evaluarán los siguientes escenarios:

1. Si es menor/igual al número del nodo raíz, entonces se guarda la actual raíz en una variable auxiliar, se actualiza la raíz con el nuevo nodo y este apunta ahora al auxiliar.
2. Si es mayor al número del nodo final, este último apuntará al nuevo nodo y se actualizará el nodo final.
3. Si no cumple con ninguna de las anteriores, entonces se procede a recorrer la lista desde el nodo raíz hasta encontrar un nodo que contenga un número mayor o igual y sirva como referencia para la ubicación. Cuando se encuentra este nodo, el nodo anterior estará apuntando al nuevo nodo y este, a su vez, apuntará al de referencia.

Al cese del ingreso, se imprime el estado final de la lista.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Principal.cpp Lista.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./ej1

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* Clarificación de algunos conceptos: https://www.geeksforgeeks.org/linked-list-set-1-introduction/
