#include <iostream>
using namespace std;

// Clases.
#include "Principal.h"
#include "Lista.h"

void menu(int &opc){
    while(true){
        cout << "\n¿Desea ingresar otro nodo? SÍ: 1, NO: 0" << endl;
        cout << "Opción: ";
        if(cin >> opc){
            break;
        }
        else{
            cout << "Se ha producido un error. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }
}

void ingreso(int &numero){
    // Recepción de un número entero.
    while(true){
        cout << "\nIngrese un número: ";
        if(cin >> numero){
            // Si el número ingresado es entero, se rompe el ciclo.
            break;
        }
        else{
            cout << "El valor es inválido. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }
}


int main(){
    Principal principal = Principal();
    Lista *lista = principal.get_lista();
    int numero = 0;
    int opc = 0;

    ingreso(numero);
    // Se añade primer nodo a la lista.
    lista->crear_nodo(numero);
    // Impresión de su estado.
    lista->imprimir();

    // Ciclo para agregar los nodos siguientes.
    while(true){
        menu(opc);

        if(opc == 1){
            ingreso(numero);
            lista->crear_nodo(numero);
            lista->imprimir();
        }
        else if(opc == 0){
            cout << "\nEstado final: " << endl;
            lista->imprimir();
            break;
        }

        else{
            cout << "No válido." << endl;
        }
    }

    return 0;
}
