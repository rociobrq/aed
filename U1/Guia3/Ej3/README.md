# Ejercicio 3

Para este programa, se permite el ingreso de números enteros para generar una lista enlazada ordenada. La lista tiene como atributos el nodo raíz y el nodo final de la misma. En base a esto, el primer número ingresado será tanto el nodo raíz como el nodo final. Seguidamente, el ingreso de números continuará siempre que el usuario seleccione la opción 1, enseñando en cada oportunidad el estado actual de la lista. Básicamente, en cada ingreso se evaluarán los siguientes escenarios:

1. Si es menor/igual al número del nodo raíz, entonces se guarda la actual raíz en una variable auxiliar, se actualiza la raíz con el nuevo nodo y este apunta ahora al auxiliar.
2. Si es mayor al número del nodo final, este último apuntará al nuevo nodo y se actualizará el nodo final.
3. Si no cumple con ninguna de las anteriores, entonces se procede a recorrer la lista desde el nodo raíz hasta encontrar un nodo que contenga un número mayor o igual y sirva como referencia para la ubicación. Cuando se encuentra este nodo, el nodo anterior estará apuntando al nuevo nodo y este, a su vez, apuntará al de referencia.

Al cese del ingreso, se identificarán aquellos números faltantes. Por ejemplo, en la lista 1-3-5, faltaría el número 2 (entre 1 y 3) y 4 (entre 3 y 5). Para realizar esto, se utiliza el método detectar() de la clase Lista para recorrer los nodos e identificar si entre sus números hay diferencias. Se tienen los siguientes casos:

* Si hay diferencia mayor a 1, se llama al método completar() con los parámetros referidos a la cantidad de nodos que faltan y los nodos entre los que se deben agregar. Para concretar esto, se guarda en una variable auxiliar el nodo previo, desde el que se deben comenzar a añadir los demás. Entonces, se ejecuta un ciclo que permitirá agregar los nodos, creando en cada iteración uno nuevo para el que se determinará el número a partir del número anterior y estará apuntando a NULL. Luego, el nodo previo apuntará al nuevo nodo, y se actualiza la variable auxiliar con este último. Al terminar el ciclo, el último nodo generado apuntará al nodo superior, terminando así de enlazar.
* Si no se cumple lo anterior, se sigue recorriendo la lista.
* Si se llega al nodo final, se termina el ciclo.

Finalmente, se muestra la lista generada.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Principal.cpp Lista.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./ej3

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.
