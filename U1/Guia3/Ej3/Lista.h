#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

// Estructura del nodo.
typedef struct _Nodo {
    int num;
    struct _Nodo *sig;
} Nodo;


class Lista{
    private:
        Nodo *raiz = NULL;
        Nodo *final = NULL;

    public:
        // Constructor.
        Lista();

        // Métodos.
        // Crea un nodo recibiendo un número entero.
        void crear_nodo(int numero);
        // Detecta posiciones donde faltan números.
        void detectar();
        // Agrega nodos faltantes.
        void completar(int dif, Nodo *previo, Nodo *tmp);
        // Imprime el estado de la lista.
        void imprimir();


};
#endif
