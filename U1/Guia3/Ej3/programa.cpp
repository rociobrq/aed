#include <iostream>
using namespace std;

// Clases.
#include "Principal.h"
#include "Lista.h"


void menu(int &opc){
    while(true){
        cout << "\n¿Desea ingresar otro nodo? SÍ: 1, NO: 0" << endl;
        cout << "Opción: ";
        if(cin >> opc){
            // Ingreso correcto rompe el ciclo.
            break;
        }
        else{
            cout << "Se ha producido un error. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }
}


void ingreso(int &numero){
    while(true){
        cout << "\nIngrese un número: ";
        if(cin >> numero){
            // Ingreso correcto de número entero.
            break;
        }
        else{
            cout << "El valor es inválido. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }
}


int main(){
    Principal principal = Principal();
    Lista *lista = principal.get_lista();
    int numero = 0;
    int opc = 0;

    // Se añade primer nodo.
    ingreso(numero);
    lista->crear_nodo(numero);
    lista->imprimir();

    while(true){
        menu(opc);

        if(opc == 1){
            // Se siguen añadiendo nodos mientras opcíon sea 1.
            ingreso(numero);
            lista->crear_nodo(numero);
            lista->imprimir();
        }
        else if(opc == 0){
            // Se rompe el ciclo.
            break;
        }

        else{
            cout << "No válido." << endl;
        }
    }

    // Al finalizar ingreso, se procede a completar números faltantes.
    lista->detectar();
    cout << "Lista final: ";
    lista->imprimir();

    return 0;
}
