#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        int *pila;
        int max;
        int tope;

    public:
        /* Constructores */
        Pila();
        Pila(int *pila, int max, int tope);

        /* Métodos */
        void inicializar(int max);
        void set_tope(int tope);
        int get_max();
        int get_tope();
        int* get_pila();
};
#endif
