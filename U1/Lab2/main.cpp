#include <iostream>
using namespace std;
#include "Pila.h"


void menu(){
    cout << "\n[1] Agregar/push" << endl;
    cout << "[2] Remover/pop" << endl;
    cout << "[3] Ver pila" << endl;
    cout << "[0] Salir" << endl;
}

void imprimir(int tope, int *pila_array){
    cout << "\n";

    for(int i = tope; i > 0; i--){
        cout << "|" << pila_array[i] << "|" << endl ;
    }
}

void pila_vacia(int tope, bool &band){
    if(tope == 0){
        band = true;
    }
    else{
        tope = false;
    }
}

void pop(Pila &pila_obj, int tope){
    bool band;

    pila_vacia(tope, band);

    if(band == true){
        cout << "Subdesbordamiento, pila vacía" << endl;
    }
    else{
        int *pila_array= pila_obj.get_pila();
        int dato = pila_array[tope];
        cout << "Se ha eliminado el dato " << dato << endl;
        pila_obj.set_tope(tope - 1);
    }
}

void pila_llena(int tope, int max, bool &band){
    if(tope == max){
        band = true;
    }
    else{
        band = false;
    }
}

void push(Pila &pila_obj, int &tope, int max,  int dato){
    bool band;

    pila_llena(max, tope, band);

    if(band == true){
        cout << "Desbordamiento, pila llena" << endl;
    }
    else{
        pila_obj.set_tope(tope + 1);
        int *pila_array= pila_obj.get_pila();
        pila_array[tope + 1] = dato;
    }
}

void control(int max_pila, Pila pila_obj){
    int opcion, max = pila_obj.get_max();

    while(true){
        // Tope se actualiza en cada ciclo.
        int tope = pila_obj.get_tope();

        // Se muestran opciones disponibles.
        menu();
        cout << "\nOpción: ";

        if(cin >> opcion){

            // Agregar elemento.
            if(opcion == 1){
                int dato;

                cout << "\nElemento a añadir: ";
                // Validar dato (entero).
                if(cin >> dato){
                    push(pila_obj, tope, max, dato);
                }
                else{
                    cout << "¡Error! No se ha podido añadir elemento." << endl;
                }
            }

            // Eliminar elemento.
            else if(opcion == 2){
                pop(pila_obj, tope);
            }

            // Ver pila.
            else if(opcion == 3){
                bool band;
                int *pila_array= pila_obj.get_pila();

                pila_vacia(tope, band);

                if(band == true){
                    cout << "¡La pila está vacía!" << endl;
                }
                else{
                    imprimir(tope, pila_array);
                }
            }

            // Salir.
            else if(opcion == 0){
                break;
            }

            else{
                cout << "Opción no válida. " << endl;
            }
        }
        else{
            cout << "¡Se ha producido un error!" << endl;
            cin.clear();
            cin.ignore();
        }
    }
}

int main(){
    int max_pila;
    Pila pila_obj = Pila();

    // Inicializar pila.
    while(true){
        cout << "\nCantidad máxima de la pila: ";
        // Se valida el tamaño máximo de la pila para inicializar el objeto.
        if(cin >> max_pila && max_pila > 1){
            pila_obj.inicializar(max_pila);
            break;
        }
        else{
            cout << "¡Se ha producido un error!" << endl;
            cin.clear();
            cin.ignore();
        }
    }

    // Manipulación de la pila.
    control(max_pila, pila_obj);

    return 0;
}
