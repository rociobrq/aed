#include <iostream>
using namespace std;
#include "Pila.h"

/* Constructores */
Pila::Pila(){
    int *pila;
    int max;
    int tope;
}

Pila::Pila(int *pila, int max, int tope){
    this->pila = pila;
    this->max = max;
    this->tope = tope;
}

/* Métodos */
void Pila::inicializar(int max){
    this->max = max;
    this->pila = new int[max];
    this->tope = 0;
}

void Pila::set_tope(int tope){
    // Tope es constante modificado e informa el estado de la pila.
    this->tope = tope;
}

int Pila::get_max(){
    return this->max;
}

int Pila::get_tope(){
    return this->tope;
}

int* Pila::get_pila(){
    return this->pila;
}
