# Pila

El programa permite la manipulación de una pila, a través del trabajo con una clase Pila que tiene como atributo el arreglo de enteros, el máximo y el tope. Para generar la pila, se pide el tamaño (máximo) de la misma: si el tamaño corresponde a un entero, se procede a crear la pila y se muestra un menú con las opciones disponibles; en caso contrario, no se genera la lista y se informa el error. En cuanto a las opciones, al agregar un elemento se revisa que el elemento sea entero y luego la función push se encarga de llamar a la función pila_llena para determinar si es posible añadir otro al arreglo. Ante esto último, si la pila no está llena, entonces se procede a añadir el elemento. Luego, otra opción disponible es la de eliminar un elemento, la cual es realizada por la función pop, que llama a la función pila_vacia para determinar si hay elementos en el arreglo. En el caso de que existan, se elimina el último elemento añadido. Seguidamente, existe una función para visualizar la pila y también otra para salir del programa.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ main.cpp Pila.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./main

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* A https://www.tutorialspoint.com/cplusplus/cpp_return_arrays_from_functions.htm
