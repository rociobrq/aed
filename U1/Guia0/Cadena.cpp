#include <iostream>
using namespace std;
#include <list>
#include "Cadena.h"

/* Constructores */
Cadena::Cadena(){
    string letra;
    list<Aminoacido> aminoacidos;
}

Cadena::Cadena(string letra, list<Aminoacido> aminoacidos){
    this->letra = letra;
    this->aminoacidos = aminoacidos;
}

/* Métodos */
void Cadena::set_letra(string letra){
    this->letra = letra;
}

void Cadena::add_aminoacido(Aminoacido aminoacido){
    // Añade nuevo objeto Aminoacido al final de la lista.
    aminoacidos.push_back(aminoacido);
}

string Cadena::get_letra(){
    return this->letra;
}

list<Aminoacido> Cadena::get_aminoacidos(){
    return this->aminoacidos;
}
