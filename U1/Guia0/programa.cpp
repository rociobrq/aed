#include <iostream>
using namespace std;
#include <list>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

void menu_principal(){
    cout << "\nMENÚ PRINCIPAL" << endl;
    cout << "1. Añadir proteína" << endl;
    cout << "2. Ver proteínas" << endl;
    cout << "3. Salir" << endl;
}

void menu_proteinas(){
    cout << "\nOpciones:";
    cout << endl << "1. Visualizar detalle de una proteína." << endl;
    cout << "2. Retroceder." << endl;
}

void imprimir_listado(list<Proteina> proteinas){
    int contador = 1;

    cout << "\nLISTADO DE PROTEÍNAS\n";

    // Se imprime información básica por cada proteína contenida en lista.
    for(Proteina p: proteinas){
        cout << contador;
        cout << ". " << p.get_nombre() << endl;
        cout << "- ID: " << p.get_id() << endl;
        cout << "- N° de cadenas: " << p.get_cadenas().size() << endl;
        contador = contador++;
    }
}

void imprimir_proteina(Proteina proteina){
    int i = 1;
    Coordenada xyz;

    // Imprime detalle de proteína seleccionada.
    cout << "\nPROTEÍNA ID " << proteina.get_id() << endl;
    cout << "Nombre: " << proteina.get_nombre() << endl;

    // Ciclos permiten acceder a atributos de todos los objetos.
    for(Cadena c: proteina.get_cadenas()){
        cout << "CADENA " << c.get_letra() << endl;

        for(Aminoacido aa: c.get_aminoacidos()){
            cout << "AMINOÁCIDO " << aa.get_nombre();
            cout << "\t" << aa.get_numero() << endl;

            for(Atomo at: aa.get_atomos()){
                xyz = at.get_coordenada();
                cout << left << "ÁTOMO ";
                cout.width(5); cout << left << at.get_numero();
                cout.width(5); cout << left << at.get_nombre();
                cout << "(" << xyz.get_x();
                cout << ", " << xyz.get_y() << ", ";
                cout << xyz.get_z() << ")" << endl;
            }
        }
        cout << "\n";
    }
}

Coordenada crear_coordenada(){
    float x, y, z;
    Coordenada obj_coordenada;

    while(true){
        cout << "----- Coordenada X: ";
        cin >> x;

        cout << "----- Coordenada Y: ";
        cin >> y;

        cout << "----- Coordenada Z: ";
        cin >> z;

        if(cin.fail()){
            cout << "Se ha producido un error. Intente nuevamente. \n";
            cin.clear();
            cin.ignore();
        }
        else{
            obj_coordenada.set_x(x);
            obj_coordenada.set_y(y);
            obj_coordenada.set_z(z);
            break;
        }
    }
    return obj_coordenada;
}

Atomo crear_atomo(){
    string nombre;
    int numero;
    Atomo obj_atomo;

    obj_atomo = Atomo();

    cout << "----- Ingresar átomo... " << endl;

    // Añadir nombre.
    cout << "----- Nombre: ";
    cin >> nombre;
    obj_atomo.set_nombre(nombre);

    while(true){
        // Añadir número.
        cout << "----- Número: ";
        cin >> numero;
        if(!cin.fail()){
            obj_atomo.set_numero(numero);
            break;
        }
        else{
            cout << "Debe ingresar un número.\n";
            cin.clear();
            cin.ignore();
        }
    }
    return obj_atomo;
}

Aminoacido crear_aminoacido(){
    string nombre;
    int numero;
    Aminoacido obj_aa;

    obj_aa = Aminoacido();

    cout << "--- Ingresar aminoácido..." << endl;

    // Obtención del nombre del aminoácido.
    cout << "--- Nombre aminoácido: ";
    cin >> nombre;
    obj_aa.set_nombre(nombre);

    while(true){
        // Añadir número/ubicación.
        cout << "--- Número aminoácido: ";
        cin >> numero;

        if(!cin.fail()){
            obj_aa.set_numero(numero);
            break;
        }
        else{
            cout << "Debe ingresar un número.\n";
            cin.clear();
            cin.ignore();
        }
    }
    return obj_aa;
}

Cadena crear_cadena(list<Cadena> cadenas) {
    int flag;
    string letra;
    Cadena obj_cadena;

    obj_cadena = Cadena();

    while(true){
        flag = 0;
        // Obtener letra.
        cout << "- Letra de cadena: ";
        cin >> letra;

        if(cadenas.empty()){
            obj_cadena.set_letra(letra);
            break;
        }
        else{
            for(Cadena c: cadenas){
                // Variable cambia valor al encontrar cadena con igual letra.
                if(letra == c.get_letra()){
                    flag = 1;
                }
            }
            if(flag == 0){
                obj_cadena.set_letra(letra);
                break;
            }
            else{
                cout << "Ingreso inválido. Intente nuevamente." << endl;
                cin.clear();
                cin.ignore();
            }
        }
    }
    return obj_cadena;
}

Proteina crear_proteina(list<Proteina> lista_proteinas){
    string nombre, id;
    int i, j, k, num_cadena, num_aa, num_atomo, flag;
    Proteina nueva_proteina;
    Cadena nueva_cadena;
    Aminoacido nuevo_aa;
    Atomo nuevo_atomo;
    Coordenada nueva_coordenada;

    cout << "\nINGRESO NUEVA PROTEÍNA" << endl;

    nueva_proteina = Proteina();

    // Recibir nombre.
    cout << "Nombre: ";
    cin >> nombre;
    nueva_proteina.set_nombre(nombre);

    // Recibir ID.
    while(true){
        flag = 0;
        cout << "ID: ";
        cin >> id;

        // Verificar que la ID sea única.
        for(Proteina p: lista_proteinas){
            if(id == p.get_id()){
                flag = 1;
            }
        }

        if(flag == 0){
            break;
        }
        else{
            cout << "La ID ingresada ya está registrada.\n";
            cin.clear();
            cin.ignore();
        }
    }

    nueva_proteina.set_id(id);

    // Consultar cantidad de cadenas a añadir.
    cout << "Número de cadenas: ";
    cin >> num_cadena;

    for(i = 0; i < num_cadena; i++){
        nueva_cadena = crear_cadena(nueva_proteina.get_cadenas());
        // Determinar cantidad de aminoácidos de la cadena.
        cout << "-- Número de aminoácidos: ";
        cin >> num_aa;

        for(j = 0; j < num_aa; j++){
            nuevo_aa = crear_aminoacido();
            // Determinar cantidad de átomos del aminoácido.
            cout << "---- Número de átomos: ";
            cin >> num_atomo;

            for(k = 0; k < num_atomo; k++){
                nuevo_atomo = crear_atomo();
                nueva_coordenada = crear_coordenada();
                nuevo_atomo.set_coordenada(nueva_coordenada);
                nuevo_aa.add_atomo(nuevo_atomo);
            }
            nueva_cadena.add_aminoacido(nuevo_aa);
        }
        nueva_proteina.add_cadena(nueva_cadena);
    }
    return nueva_proteina;
}

Proteina agregar_pbase(){
    Proteina p_base;
    Cadena  cad;
    Aminoacido aa1;
    Atomo at1, at2;
    Coordenada coor1, coor2;

    p_base = Proteina();
    p_base.set_nombre("Mioglobina");
    p_base.set_id("2SPN");
    cad = Cadena();
    cad.set_letra("A");
    aa1 = Aminoacido();
    aa1.set_nombre("MET");
    aa1.set_numero(1);
    at1 = Atomo();
    at1.set_nombre("N");
    at1.set_numero(1);
    coor1 = Coordenada();
    coor1.set_x(24.460);
    coor1.set_y(8.503);
    coor1.set_z(-9.146);
    at1.set_coordenada(coor1);
    aa1.add_atomo(at1);
    at2 = Atomo();
    at2.set_nombre("C");
    at2.set_numero(2);
    coor2 = Coordenada();
    coor2.set_x(25.830);
    coor2.set_y(10.323);
    coor2.set_z(-10.083);
    at2.set_coordenada(coor2);
    aa1.add_atomo(at2);
    cad.add_aminoacido(aa1);
    p_base.add_cadena(cad);

    return p_base;
}

int main(){
    list<Proteina> proteinas;
    int op, op_p, existencia;
    string id_p;
    Proteina p_base, obj_proteina;

    system("clear");

    // Añadir proteína base a la lista.
    p_base = agregar_pbase();
    proteinas.push_back(p_base);

    while(true){
        menu_principal();
        cin >> op;

        if(cin.fail()){
            cout << "Se ha producido un error. Intente nuevamente. \n";
            cin.clear();
            cin.ignore();
        }

        // Opción de ingresar proteína.
        if(op == 1){
            obj_proteina = crear_proteina(proteinas);

            // Se añade a lista.
            proteinas.push_back(obj_proteina);
        }

        // Opción para ver información de proteínas.
        else if(op == 2) {
            if(!proteinas.empty()){
                imprimir_listado(proteinas);

                // Sub-menú para visualizar proteína en detalle.
                menu_proteinas();
                cin >> op_p;

                if(op_p == 1){
                    existencia = 0;
                    cout << "Ingresar ID: ";
                    cin >> id_p;

                    // Ciclo para encontrar ID de la proteína.
                    for(Proteina p: proteinas){
                        if(p.get_id() == id_p){
                            // Variable para señalar coincidencia de ID.
                            existencia = 1;
                            imprimir_proteina(p);
                        }
                    }

                    if(existencia == 0){
                        cout << "ID '" << id_p << "'sin coincidencias.\n";
                    }
                }
            }

            else{
                cout << "Lista vacía. \n";
            }
        }

        // Opción para salir del programa.
        else if(op == 3){
            break;
        }
    }
    return 0;
}
