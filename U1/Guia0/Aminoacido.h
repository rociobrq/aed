#include <iostream>
using namespace std;
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido{
    private:
        string nombre;
        int numero;
        list<Atomo> atomos;

    public:
        /* Constructores */
        Aminoacido();
        Aminoacido(string nombre, int numero, list<Atomo> atomos);

        /* Métodos */
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atomo(Atomo atomo);
        string get_nombre();
        int get_numero();
        list<Atomo> get_atomos();
};
#endif
