#include <iostream>
using namespace std;
#include "Coordenada.h"

#ifndef ATOMO_H
#define ATOMO_H

class Atomo{
    private:
        string nombre;
        int numero;
        Coordenada coordenada;

    public:
        /* Constructores */
        Atomo();
        Atomo(string nombre, int numero, Coordenada coordenada);

        /* Métodos */
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(Coordenada coordenada);
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();
};
#endif
