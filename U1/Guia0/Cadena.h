#include <iostream>
using namespace std;
#include "Aminoacido.h"

#ifndef CADENA_H
#define CADENA_H

class Cadena {
    private:
        string letra;
        list<Aminoacido> aminoacidos;

    public:
        /* Constructores */
        Cadena();
        Cadena(string letra, list<Aminoacido> aminoacidos);

        /* Métodos */
        void set_letra(string letra);
        void add_aminoacido(Aminoacido aminoacido);
        string get_letra();
        list<Aminoacido> get_aminoacidos();
};
#endif
