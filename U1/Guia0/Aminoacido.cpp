#include <iostream>
using namespace std;
#include <list>
#include "Aminoacido.h"

/* Constructores */
Aminoacido::Aminoacido(){
    string nombre;
    int numero;
    list<Atomo> atomos;
}

Aminoacido::Aminoacido(string nombre, int numero, list<Atomo> atomos){
    this->nombre = nombre;
    this->numero = numero;
    this->atomos = atomos;
}

/* Métodos */
void Aminoacido::set_nombre(string nombre){
    this->nombre = nombre;
}

void Aminoacido::set_numero(int numero){
    this->numero = numero;
}

void Aminoacido::add_atomo(Atomo atomo){
    // Añade objeto Atomo al final de la lista.
    atomos.push_back(atomo);
}

string Aminoacido::get_nombre(){
    return this->nombre;
}

int Aminoacido::get_numero(){
    return this->numero;
}

list<Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}
