#include <iostream>
using namespace std;
#include <list>
#include "Proteina.h"

/* Constructores */
Proteina::Proteina(){
    string nombre;
    string id;
    list<Cadena> cadenas;
}

Proteina::Proteina(string nombre, string id, list<Cadena> cadenas){
    this->nombre = nombre;
    this->id = id;
    this->cadenas = cadenas;
}

/* Métodos */
void Proteina::set_nombre(string nombre){
    this->nombre = nombre;
}

void Proteina::set_id(string id){
    this->id = id;
}

void Proteina::add_cadena(Cadena cadena){
    // Añade nuevo objeto Cadena al final de la lista.
    cadenas.push_back(cadena);
}

string Proteina::get_nombre(){
    return this->nombre;
}

string Proteina::get_id(){
    return this->id;
}

list<Cadena> Proteina::get_cadenas(){
    return this->cadenas;
}
