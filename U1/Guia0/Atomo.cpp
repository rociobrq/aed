#include <iostream>
using namespace std;
#include <list>
#include "Atomo.h"

/* Constructores */
Atomo::Atomo(){
    string nombre;
    int numero;
    Coordenada coordenada;
}

Atomo::Atomo(string nombre, int numero, Coordenada coordenada){
    this->nombre = nombre;
    this->numero = numero;
    this->coordenada = coordenada;
}

/* Métodos */
void Atomo::set_nombre(string nombre){
    this->nombre = nombre;
}

void Atomo::set_numero(int numero){
    this->numero = numero;
}

void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}

string Atomo::get_nombre(){
    return this->nombre;
}

int Atomo::get_numero(){
    return this->numero;
}

Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}
