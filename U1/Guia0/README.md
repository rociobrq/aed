# Registro proteínas

El programa se basa en el registro de proteínas, para lo que se debe contar con información sobre su nombre, ID, cadenas, secuencias aminoacídicas por cada cadena y las coordenadas de los átomos que las conforman. En añadidura, se permite posteriormente la búsqueda de proteínas mediante su ID, permitiendo observar en detalle toda la información inicialmente registrada.

## Comenzando

El menú principal indica tres opciones: 1) añadir proteína, 2) ver proteínas y 3) salir. Primeramente, al elegir la opción 1 el usuario debe ingresar toda la información necesaria para registrar la proteína. Por otra parte, la segunda opción permite ver un listado de las proteínas registradas (imprimiendo nombre, ID y n° de cadenas). Luego, el usuario tiene la posibilidad de ver el detalle de una proteína específica, para lo cual debe ingresar el ID de la misma y, si hay coincidencias, se procede a imprimir toda la información. Finalmente, la tercera opción permite terminar el programa.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp -o programa

Luego, para ejecutar el programa se debe utilizar:
* ./programa

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Guia0

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* https://stackoverflow.com/questions/5131647/why-would-we-call-cin-clear-and-cin-ignore-after-reading-input
* http://www.cplusplus.com/reference/list/list/
