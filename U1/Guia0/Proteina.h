#include <iostream>
using namespace std;
#include "Cadena.h"

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina{
    private:
        string nombre;
        string id;
        list<Cadena> cadenas;

    public:
        /* Constructores */
        Proteina();
        Proteina(string nombre, string id, list<Cadena> cadenas);

        /* Métodos */
        void set_nombre(string nombre);
        void set_id(string id);
        void add_cadena(Cadena cadena);
        string get_id();
        string get_nombre();
        list<Cadena> get_cadenas();
};
#endif
