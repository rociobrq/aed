#include <iostream>
using namespace std;

#ifndef FRASE_H
#define FRASE_H

class Frase {
    private:
        string contenido;
        int numlower;
        int numupper;
    public:
        /* Constructores */
        Frase();
        Frase(string contenido, int numlower, int numupper);

        /* Métodos */
        void set_contenido(string contenido);
        void set_numlower(int numlower);
        void set_numupper(int numupper);
        string get_contenido();
        int get_numlower();
        int get_numupper();
};
#endif
