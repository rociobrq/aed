#include <iostream>
using namespace std;
#include "Frase.h"

/* Constructores */
Frase::Frase(){
    string contenido;
    int numlower;
    int numupper;
}

Frase::Frase(string contenido, int numlower, int numupper){
    this->contenido = contenido;
    this->numlower = numlower;
    this ->numupper = numupper;
}

/* Métodos */
void Frase::set_contenido(string contenido){
    this->contenido = contenido;
}

void Frase::set_numlower(int numlower){
    this->numlower = numlower;
}

void Frase::set_numupper(int numupper){
    this->numupper = numupper;
}

string Frase::get_contenido(){
    return this->contenido;
}

int Frase::get_numlower(){
    return this->numlower;
}

int Frase::get_numupper(){
    return this->numupper;
}
