#include <iostream>
using namespace std;
#include <cctype>
#include <string>
#include "Frase.h"


void crear_objetos(Frase* frases, string* inputs, int num){
    for(int i = 0; i < num; i++){
        // Ingreso de la frase.
        frases[i].set_contenido(inputs[i]);
        int lower = 0;
        int upper = 0;

        cout << inputs[i].size() << " " << inputs[i] << endl;

        // Se recorre cada caracter del string para determinar mayús o minus.
        for(int j = 0; j < inputs[i].size(); j++){
            if(islower(inputs[i][j])){
                lower = lower + 1;
            }
            else if(isupper(inputs[i][j])){
                upper = upper + 1;
            }
        }

        // Se ingresa número de minísculas y mayúsculas.
        frases[i].set_numlower(lower);
        frases[i].set_numupper(upper);
    }
}

void array_inputs(string* inputs, int num){
    int i = 0;
    cin.ignore();

    // Recibir todas las frases, validando ingreso.
    while(i < num){
        cout << "Ingresar frase: ";
        getline(cin, inputs[i]);

        // Ingreso inválido, limpia buffer.
        if(inputs[i].empty() || inputs[i] == " "){
            cout << "String vacío..." << endl;
            cin.clear();
            cin.ignore();
        }
        else{
            i++;
        }
    }
}

int main(){
    int num;

    // Se obtiene cantidad de frases a ingresar.
    while(true){
        cout << "Número de frases a ingresar: ";
        cin >> num;

        if(cin.fail() || num < 1){
            cout << "¡Ha ocurrido un error!" << endl;
            cin.clear();
            cin.ignore();
        }
        else{
            break;
        }
    }

    // Se establece arreglo para recibir las frases.
    string inputs[num];
    array_inputs(inputs, num);

    // Se crean los objetos de la clase.
    Frase frases[num] = {Frase()};
    crear_objetos(frases, inputs, num);

    // Se imprimen frases y el número de mayúsculas y minúsculas.
    cout << "\nFRASES" << endl;

    for(int i = 0; i < num; i++){
        int j = i + 1;
        cout << j << ". '" << frases[i].get_contenido() << "'" <<  endl;
        cout << "\tMinúsculas: " << frases[i].get_numlower() << endl;
        cout << "\tMayúsculas: " << frases[i].get_numupper() << endl;
    }

    return 0;
}
