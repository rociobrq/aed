# Ejercicio 2

El usuario determina un número de frases a ingresar. Luego se le pide que ingrese cada una de estas, aplicando la validación respectiva en el programa. Luego, se procede a crear objetos de la clase Frase y se utilizan los métodos set para determinar los atributos: contenido (frase como tal), numlower (número de minúsculas), numupper (número de mayúsculas). Finalmente, se imprime cada frase con la cantidad de mayúsculas y minúsculas, terminando la ejecución.


## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ main.cpp Frase.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./main

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab1/Ej2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* A http://www.cplusplus.com/reference/string/string/empty/
