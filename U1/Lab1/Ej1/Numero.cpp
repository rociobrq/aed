#include <iostream>
using namespace std;
#include "Numero.h"

/* Constructores */
Numero::Numero(){
    int numero;
    int cuadrado;
}

Numero::Numero(int numero, int cuadrado) {
    this->numero = 0;
    this->cuadrado = 0;
}

/* Métodos */
void Numero::set_numero(int numero){
    this->numero = numero;
}

void Numero::set_cuadrado(int cuadrado){
    this->cuadrado = cuadrado;
}

int Numero::get_numero(){
    return this->numero;
}

int Numero::get_cuadrado(){
    return this->cuadrado;
}
