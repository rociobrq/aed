#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero{
    private:
        int numero = 0;
        int cuadrado = 0;

    public:
        /* Constructores */
        Numero();
        Numero(int numero, int cuadrado);

        /* Métodos */
        void set_numero(int numero);
        void set_cuadrado(int cuadrado);
        int get_numero();
        int get_cuadrado();
};
#endif
