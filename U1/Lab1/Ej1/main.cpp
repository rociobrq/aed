#include <iostream>
using namespace std;
#include "Numero.h"


int calcular_suma(int suma, Numero* numeros, int n){
    // Obtiene suma de los cuadrados mediante métodos de la clase.
    for(int i = 0; i < n; i++){
        suma += numeros[i].get_cuadrado();
    }
    return suma;
}

void imprimir_numeros(Numero* numeros, int n){
    cout << "Números y cuadrados" << endl;
    for(int i = 0; i < n; i++){
        cout << "\t" << numeros[i].get_numero() << "->";
        cout << numeros[i].get_cuadrado() << endl;
    }
}

void crear_objetos(Numero* numeros, int* vector, int n){
    for(int i = 0; i < n; i++){
        numeros[i].set_numero(vector[i]);
        numeros[i].set_cuadrado(vector[i] * vector[i]);
    }
}

void llenar_vector(int* vector, int n){
    // Se llena vector con números consecutivos.
    for(int i = 0; i < n; i++){
        vector[i] = i + 1;
    }

}

int main(int argc, char **argv){
    int n = atoi(argv[1]), vector[n], suma;
    Numero numeros[n] = {Numero()};

    // Completar arreglo con valores desde 1 a n.
    llenar_vector(vector, n);

    // Definir objetos a partir de arreglo.
    crear_objetos(numeros, vector, n);

    imprimir_numeros(numeros, n);

    // Calcular suma de los cuadrados.
    suma = calcular_suma(suma, numeros, n);

    cout << "La suma de los cuadrados de los números es: " << suma << endl;

    return 0;
}
