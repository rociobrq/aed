# Ejercicio 1

Al ejecutar este programa se permite obtener la suma de los cuadrados de una serie de números. Primeramente, el usuario debe ejecutar el programa introduciendo un número. Luego, un arreglo de tipo entero se llenará desde el n° 1 hasta el número ingresado por el usuario. Esto permite construir los objetos de clase Numero, con los métodos set, los cuales tendrán como atributo el número y el cuadrado respectivo. Luego, se procede a imprimir cada número con su cuadrado, utilizando los métodos get. Finalmente, se suman todos los cuadrados y se imprime el resultado.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ main.cpp Numero.cpp -o main

Luego, para ejecutar el programa se debe utilizar ./main incluyendo un número entero para ejecutar el programa:
* ./main entero

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab1/Ej1

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* A https://www.geeksforgeeks.org/command-line-arguments-in-c-cpp/
