# Ejercicio 3

Este programa permite el ingreso de un determinado número de clientes, permitiendo, además, que registrar el nombre, el teléfono, el saldo y el estado (moroso o no). Primeramente, se pide el número, para luego avanzar en la recepción de los datos requeridos. Estos datos son validados por funciones que retornan true o false, por lo que únicamente si todos los datos han sido ingresado correctamente se permite la creación del objeto Cliente (todos estos objetos se almacenan en un arreglo). Luego del ingreso exitoso de todos los clientes señalados inicialmente, se muestra un menú con tres opciones, siendo estas: lista de morosos, lista de no morosos y salir. Ante este último punto, tanto para la primera como para la segunda opción, el usuario luego podrá imprimir todos los clientes (y sus datos) que cumplan con el estado requerido. La opción salir permitirá terminar la ejecución.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ main.cpp Cliente.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./main

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab1/Ej3

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* A http://www.cplusplus.com/reference/cctype/toupper/
