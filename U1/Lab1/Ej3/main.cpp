#include <iostream>
using namespace std;
#include <string>
// #include <array>
#include "Cliente.h"


void menu(){
    cout << "\nOPCIONES" << endl;
    cout << "1. Lista de morosos" << endl;
    cout << "2. Lista de no morosos" << endl;
    cout << "3. Salir" << endl;
}

void imprimir(Cliente* clientes, int num, int estado){
    cout << "\nLISTADO";

    for(int i = 0; i < num; i++){
        // Se imprime listado de clientes en base a los dos grupos.
        if(clientes[i].get_moroso() == estado){
            cout << "\nNOMBRE:" << clientes[i].get_nombre() << endl;
            cout << "TELÉFONO: " << clientes[i].get_telefono() << endl;
            cout << "SALDO: " << clientes[i].get_saldo() << endl;

            if(clientes[i].get_moroso() == 0){
                cout << "ESTADO: NO MOROSO";
            }

            else{
                cout << "ESTADO: MOROSO";
            }
        }
    }
}

bool validar_string(string str){
    // Valida ingreso correcto (que tenga contenido).
    if(str.empty() || str == " "){
        return false;
    }
    else{
        return true;
    }
}

bool validar_int(int integer){
    if(integer < 0){
        return false;
    }
    else{
        return true;
    }
}

void mayus(string &str){
    // Convierte cada caracter en mayúscula recorriendo el string.
    for(int j = 0; j < str.size(); j++){
        str[j] = toupper(str[j]);
    }
}

void ingresar_datos(Cliente* clientes, int num){
    string nombre, tel;
    int saldo, i = 0;
    bool moroso;
    cin.ignore();

    // Ciclo se efectúa hasta obtener el número correcto de clientes.
    while(i < num){
        cout << "\nNuevo ingreso" << endl;

        // Ingreso de nombre.
        cout << "Nombre completo: ";
        getline(cin, nombre, '\n');

        // Ingreso de teléfono.
        cout << "Número de teléfono: ";
        getline(cin, tel, '\n');

        // Ingreso de saldo.
        cout << "Saldo: ";
        cin >> saldo;

        // Ingresar estado.
        cout << "Estado moroso: ";
        cin >> moroso;

        // Se validan todos los ingresos para crear el objeto cliente.
        if (validar_int(saldo) == true &&
            validar_string(nombre) == true &&
            validar_string(tel) == true &&
            validar_int(moroso) == true){

            // Determinar atributos.
            clientes[i].set_nombre(nombre);
            clientes[i].set_telefono(tel);
            clientes[i].set_saldo(saldo);
            clientes[i].set_moroso(moroso);

            // Creación exitosa.
            i++;
            cin.clear();
            cin.ignore();
        }
        else{
            cout << "¡Ha ocurrido un error!" << endl;
            cin.clear();
            cin.ignore();
        }
    }
}

int main(){
    int num;

    // Cantidad de clientes a ingresar.
    while(true){
        cout << "Cantidad de clientes: ";
        cin >> num;

        if(cin.fail() || num < 1){
            cout << "¡Se ha producido un error!";
            cin.clear();
            cin.ignore();
        }
        else{
            break;
        }
    }

    // Determinar arreglo de objetos.
    Cliente clientes[num] = {Cliente()};

    ingresar_datos(clientes, num);

    // Opciones disponibles.
    int opcion;
    while(true){
        menu();
        cin >> opcion;

        if(opcion == 1){
            imprimir(clientes, num, 1);
        }

        else if(opcion == 2){
            imprimir(clientes, num, 0);
        }

        else if(opcion == 3){
            break;
        }
        else{
            cout << "Opción inválida..." << endl;
            cin.clear();
            cin.ignore();
        }
    }

    return 0;
}
