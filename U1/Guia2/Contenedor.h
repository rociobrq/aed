#include <iostream>
using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
    private:
        int numero;
        string empresa;
        string info;

    public:
        // Constructores
        Contenedor();
        Contenedor(int numero, string empresa);

        // Métodos
        int get_numero();
        string get_empresa();
        string get_info();
};
#endif
