# Puerto seco

La ejecución de este programa inicia con el ingreso de las dimensiones del puerto. Para esto, se validan inicialmente los datos ingresados (si uno de ellos es 0, se pedirá otro valor). Luego, se procede a crear una instancia para la clase puerto, en la que se inicializará el puerto y se crearán las pilas indicadas. Seguidamente, se cuentan con tres opciones: agregar contenedor, extraer contenedor y salir. En esta línea, se debe mencionar que cada contenedor que desee añadirse debe contar con nombre de la empresa y un número, para poder identificarlo del resto. Además, para extraer un contenedor se debe mover todos los contenedores que están arriba de él a otras ubicaciones en la pila disponibles en ese momento.

En cuanto a la opción de agregar contenedor, inicialmente se chequea que el puerto no esté lleno (método puerto_lleno() de clase Puerto). Si no lo está, se procede a crear un objeto contenedor que se inicializará con la empresa y el número, y se añadirá a la pila correspondiente (método set_contenedor() de la clase Puerto). En cuanto a lo mencionado anteriormente, el ingreso de los contenedores es automático, es decir, se comienza a añadir desde la primera pila hasta que esta no cuente con más capacidad; solo ahí se procede a colocar contenedores en la segunda pila, y así sucesivamente. En caso de que la pila esté con la capacidad al máximo, se informa que no es posible añadir otro contenedor.  

En segundo lugar, referente a la extracción de un contenedor, primero se requiere saber si el puerto cuenta con contenedores o no (usando método puerto_vacio()). Si no posee contenedores, entonces se hace saber que el puerto está vacío. En cambio, si posee contenedores, lo inicial es pedirle al usuario que ingrese la empresa y el número de aquel que desea extraer. Luego, se usa el método buscar_contenedor() de Puerto, el que revisará cada pila; si se encuentra el contenedor, se guardan sus coordenadas y se llama al método extraer_contenedor(), de la misma clase. Este método revisa, como primer paso, si el contenedor está en el tope de su pila; si es así, se extrae inmediatamente; si no, se procede a calcular la cantidad de contenedores que se deben mover y se efectúa un ciclo que recorre las pilas, para realizar así los movimientos necesarios y sacar al contenedor objetivo. Ante este último punto, se destaca que el método ejecuta todos los movimientos posibles, de modo que, cuando ya no es posible cambiar un bloque de ubicación y aún no se ha podido extraer el contenedor objetivo, se avisa al usuario que la extracción no ha sido exitosa.

Finalmente, el programa permite terminar la ejecución del mismo mediante la opción [0].

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ main.cpp Puerto.cpp Pila.cpp Contenedor.cpp -o main

Luego, para ejecutar el programa se debe utilizar:
* ./main (int) (int)

Por ejemplo, si se desea ejecutar una matriz 2x5 (5 pilas con capacidad de 2 contenedores):
* ./main 2 5

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U1/Lab2

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* A https://stackoverflow.com/questions/1257744/can-i-use-break-to-exit-multiple-nested-for-loops
* https://www.educative.io/edpresso/how-to-convert-an-int-to-a-string-in-cpp
* http://www.cplusplus.com/reference/string/string/substr/
