#include <iostream>
using namespace std;
#include <string>
#include "Puerto.h"


// Constructores
Puerto::Puerto(){
    Pila *pilas;
    int npilas;
    int max;
    int coor[2];
}

Puerto::Puerto(Pila *pilas, int npilas, int max){
    this->pilas = NULL;
    this->npilas = 0;
    this->max = 0;
    this->coor[2] = {};
}

// Métodos
void Puerto::inicializar(int npilas, int max){
    // Se definen atributos.
    this->npilas = npilas;
    this->max = max;
    this->pilas = new Pila[npilas];

    // Generar pilas.
    for(int i = 0; i < npilas; i++){
        pilas[i] = Pila();
        pilas[i].inicializar(max);
    }

    imprimir_puerto();
}

void Puerto::set_contenedor(int numero, string empresa){
    // Se ingresa contenedor al puerto, pila por pila.
    for(int i = 0; i < npilas; i++){
        pilas[i].pila_llena();

        // Si la pila en cuestión está llena, se agrega a la siguiente.
        if(!pilas[i].get_band()){
            Contenedor contenedor = Contenedor(numero, empresa);
            pilas[i].push(contenedor);
            break;
        }
    }

    imprimir_puerto();
}

void Puerto::extraer_contenedor(){
    int tope = pilas[coor[1]].get_tope();

    // Se elimina contenedor si es el último de su pila.
    if(coor[0] == tope){
        pilas[coor[1]].pop();

        imprimir_puerto();
    }
    else{

        // Si no es el último, se obtiene la cantidad de contenedores sobre él.
        int a_mover = tope - coor[0];
        int contador = 0;
        int llenas = 0;
        int col = coor[1] + 1;

        // El ciclo se detiene cuando se recorrieron todas las pilas y no hay
        // posibilidad de movimiento, o si se extrajeron todos los necesarios.
        while(col != coor[1] && contador != a_mover){

            // Se modifica variable col para trabajar sin salir del puerto.
            if(col >= npilas){
                col = 0;
            }

            pilas[col].pila_llena();

            // Si la pila no está llena, se procede a añadir un contenedor.
            if(!pilas[col].get_band()){

                int tope = pilas[coor[1]].get_tope();
                pilas[coor[1]].pop();

                Contenedor *pila = pilas[coor[1]].get_pila();
                pilas[col].push(pila[tope]);
                contador++;
                imprimir_puerto();
            }

            // Pila llena.
            else{
                col++;
                llenas++;

                // Si se recorren todas las pilas posibles y todas están llenas
                // entonces no se termina tarea y se rompe el ciclo.
                if(llenas == npilas - 1){
                    cout << "No se ha podido concretar extracción" << endl;
                    break;
                }
            }
        }

        if(a_mover == contador){
            // Se extrae contenedor deseado después de quitar los de arriba.
            pilas[coor[1]].pop();
            imprimir_puerto();
        }
    }
}

void Puerto::buscar_contenedor(string numero, string empresa){
    int target = false;

    // Se recorren arreglos para encontrar el contenedor.
    for(int i = 0; i < npilas; i++){
        int tope = pilas[i].get_tope();

        // Si la pila tiene elementos, se procede a comparar.
        if(tope != -1){
            // Se obtiene el array de contenedores.
            Contenedor *pila = pilas[i].get_pila();

            for(int j = tope; j >= 0; j--){
                // Información del contenedor.
                string e = pila[j].get_empresa();
                int n = pila[j].get_numero();

                // Ante coincidencia, se cambia variable target y se guardan
                // las coordenadas del elemento.
                if(numero == to_string(n) && empresa == e){
                    this->coor[0] = j;
                    this->coor[1] = i;
                    target = true;
                    break;
                }
            }
        }
    }

    // Si se encuentra contenedor, se llama a método para extraerlo.
    if(target){
        extraer_contenedor();
    }
    else{
        cout << "No hay coincidencias." << endl;
    }

}

void Puerto::imprimir_puerto(){
    cout << "\n---PUERTO ACTUALIZADO---" << endl;

    // Obtener array de objetos Pila.
    for(int j = max - 1; j >= 0; j--){
        for(int i = 0; i < npilas; i++){

            // Completar espacios faltantes en la matriz (menos contenedores).
            if(j > pilas[i].get_tope()){
            cout << "|      |";
            }
            else{
                // Se recibe cada pila.
                Contenedor *pila = pilas[i].get_pila();

                // Obtener número y empresa del contenedor a imprimir.
                string string = pila[j].get_info();
                int caracteres = string.size();

                // Se modifica string para que quede con 6 caracteres.
                for(int h = 0; h < (6-caracteres); h++){
                    string = string + " ";
                }

                cout << "|" << string << "|";
            }
        }
        cout << "\n";
    }
}

bool Puerto::puerto_vacio(){
    int contador = 0;

    for(int i = 0; i < npilas; i++){
        pilas[i].pila_vacia();
        if(pilas[i].get_band()){
            // Contador se incrementa si la pila está vacía.
            contador++;
        }
    }

    // Si hay npilas vacías, puerto vacío.
    if(contador == npilas){
        return true;
    }
    else{
        return false;
    }
}

bool Puerto::puerto_lleno(){
    int contador = 0;

    // Recorre todas las pilas para determinar si el puerto está lleno.
    for(int i = 0; i < npilas; i++){
        pilas[i].pila_llena();
        if(pilas[i].get_band()){
            // Incrementa contador si la pila está llena.
            contador++;
        }
    }

    // Si están todas las pilas llenas, puerto lleno.
    if(contador == npilas){
        return true;
    }
    else{
        return false;
    }
}

Pila* Puerto::get_puerto(){
    return this->pilas;
}
