#include <iostream>
using namespace std;
#include <string>
#include "Pila.h"
#include "Puerto.h"
#include "Contenedor.h"


void menu(){
    cout << "\n[1] Agregar contenedor" << endl;
    cout << "[2] Extraer contenedor" << endl;
    cout << "[0] Salir" << endl;
}

bool validar_cont(int num, string emp, Puerto puerto, int max, int npilas){
    Pila *pilas = puerto.get_puerto();
    int contador = 0;

    for(int i = 0; i < npilas; i++){
        Contenedor *pila = pilas[i].get_pila();
        int tope = pilas[i].get_tope();

        for(int j = tope; j >= 0; j--){
            // Se comparan datos con cada contenedor.
            if(num == pila[j].get_numero() && emp == pila[j].get_empresa()){
                contador++;
            }
        }
    }

    if(contador == 0){
        // No se encuentran coincidencias.
        return true;
    }
    else{
        // Info del contenedor se repite.
        return false;
    }
}

void captar_cont(int &numero, string &empresa){
    while(true){

        cout << "\n---Ingreso de nuevo contenedor---" << endl;
        cin.ignore(1, '\n');

        cout << "Ingresar empresa: ";
        getline(cin, empresa);
        // Se extraen los primeros tres caracteres del nombre.
        empresa = empresa.substr(0,3);

        if(empresa.empty()){
            cout << "¡Error! No se ha ingresado empresa." << endl;
            cin.clear();
        }
        else{
            cout << "Ingresar número: ";
            cin >> numero;

            if(!cin.fail()){
                // Si empresa y número se ingresan bien, se rompe el ciclo.
                cout << "¡Ingreso de información exitoso!" << endl;
                break;
            }
            else{
                cout << "¡Se ha producido un error!" << endl;
                cin.clear();
            }
        }
    }
}


void validar_dim(int &num){

    // Se validan las dimensiones del puerto.
    while(true){
        cout << "\n---VALIDAR DIMENSIÓN---" << endl;
        // Si el num es 0 se pide otro valor, por lo que el ciclo se rompe
        // cuando el usuario ingrese un número válido.
        if(num == 0){
            cout << "Ingrese otro valor: ";
            if(cin >> num){
                cout << "Valor " << num << " registrado." << endl;
                break;
            }
        }
        else{
            cout << "El valor " << num << " es válido." << endl;
            break;
        }
    }
}

int main(int argc, char **argv){
    // Se reciben dimensiones por terminal.
    int max = atoi(argv[1]), npilas = atoi(argv[2]);
    int opcion;

    cout << "PUERTO SECO" << endl;
    // Determinar la dimensión final del puerto seco.
    validar_dim(npilas);
    cout << "Número de pilas: " << npilas << endl;
    validar_dim(max);
    cout << "Capacidad de cada pila: " << max << endl;

    // Creación del puerto seco con máx de pilas.
    Puerto puerto = Puerto();
    puerto.inicializar(npilas, max);

    // Opciones para trabajar con el puerto.
    while(true){

        menu();
        cout << "Opción: ";
        if(cin >> opcion){

            // Ingreso de un contenedor.
            if(opcion == 1){
                // Comprobar si hay espacio.
                if(!puerto.puerto_lleno()){
                    int numero = 0;
                    string empresa = "\0";

                    // Se reciben datos de contenedor.
                    captar_cont(numero, empresa);

                    if(validar_cont(numero, empresa, puerto, max, npilas)){
                        // Si el contenedor no tiene igual num y empresa que
                        // otro, se ingresa el contenedor al puerto.
                        puerto.set_contenedor(numero, empresa);
                    }
                    else{
                        cout << "El contenedor ya existe." << endl;
                    }
                }
                else{
                    cout << "El puerto está lleno." << endl;
                }
            }

            // Extracción de un contenedor.
            else if(opcion == 2){
                // Comprobar si hay contenedores para extraer.
                if(!puerto.puerto_vacio()){
                    string orden, empresa, numero;
                    cin.ignore();
                    // Ingreso de datos del contenedor a buscar.
                    cout << "Ingresar empresa: ";
                    getline(cin, empresa);
                    cout << "Ingresar número: ";
                    getline(cin, numero);

                    // Llamar a método que buscará el contenedor.
                    puerto.buscar_contenedor(numero, empresa);
                }
                else{
                    cout << "El puerto está vacío." << endl;
                }
            }
            // Salir del programa.
            else if(opcion == 0){
                break;
            }
            else{
                cout << "Opción no válida." << endl;
            }
        }
        else{
            cout << "¡Se ha producido un error!" << endl;
            cin.clear();
            cin.ignore();
        }
    }

    return 0;
}
