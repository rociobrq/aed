#include <iostream>
using namespace std;
#include "Pila.h"


// Constructores
Pila::Pila(){
    Contenedor *pila;
    int max;
    int tope;
    bool band;
}

Pila::Pila(Contenedor *pila, int max, int tope, bool band){
    this->pila = NULL;
    this->max = 0;
    this->tope = 0;
    this->band = false;
}

// Métodos
void Pila::inicializar(int max){
    this->max = max;
    this->tope = -1;
    this->pila = new Contenedor[max];
}

void Pila::pila_llena(){
    if(this->tope == max - 1){
        this->band = true;
    }
    else{
        this->band = false;
    }
}

void Pila::pila_vacia(){
    if(this->tope == -1){
        this->band = true;
    }
    else{
        this->band = false;
    }
}

void Pila::push(Contenedor contenedor){
    // Recibe objeto Contenedor para incorporarlo en el arreglo.
    this->tope = tope + 1;
    this->pila[tope] = contenedor;
}

void Pila::pop(){
    // Elimna último contenedor de la pila (se actualiza el tope).
    this->tope = tope - 1;
}

int Pila::get_tope(){
    return this->tope;
}

bool Pila::get_band(){
    return this->band;
}

Contenedor* Pila::get_pila(){
    return this->pila;
}
