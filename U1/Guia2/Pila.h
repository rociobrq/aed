#include <iostream>
using namespace std;
#include "Contenedor.h"

#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        Contenedor *pila;
        int max;
        int tope;
        bool band;

    public:
        // Constructores
        Pila();
        Pila(Contenedor *pila, int max, int tope, bool band);

        // Métodos
        void inicializar(int max);
        void pila_llena();
        void pila_vacia();
        void push(Contenedor contenedor);
        void pop();
        int get_tope();
        bool get_band();
        Contenedor* get_pila();
};
#endif
