#include <iostream>
using namespace std;
#include "Pila.h"

#ifndef PUERTO_H
#define PUERTO_H

class Puerto{
    private:
        Pila *pilas;
        int npilas;
        int max;
        int coor[2];

    public:
        // Constructores.
        Puerto();
        Puerto(Pila *pilas, int npilas, int max);

        // Métodos.
        void inicializar(int npilas, int max);
        void set_contenedor(int numero, string empresa);
        void extraer_contenedor();
        void buscar_contenedor(string numero, string empresa);
        void imprimir_puerto();
        bool puerto_lleno();
        bool puerto_vacio();
        Pila* get_puerto();
};
#endif
