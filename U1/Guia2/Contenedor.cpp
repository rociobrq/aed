#include <iostream>
using namespace std;
#include <string>
#include "Contenedor.h"


// Constructores
Contenedor::Contenedor(){
    int numero;
    string empresa;
    string info;
}

Contenedor::Contenedor(int numero, string empresa){
    this->numero = numero;
    this->empresa = empresa;
    this->info = to_string(this->numero) + "/" + this->empresa;
}

// Métodos
int Contenedor::get_numero(){
    return this->numero;
}

string Contenedor::get_empresa(){
    return this->empresa;
}

string Contenedor::get_info(){
    // Retorna todos los datos concatenados.
    return this->info;
}
