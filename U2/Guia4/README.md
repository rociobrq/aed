# Guía 4

Un árbol binario de búsqueda corresponde a una estructura en la que al ingresar números se sigue un orden específico. Cada número apunta a otros dos: a la izquierda un número menor a sí mismo y a la derecha un número mayor. En este caso, el programa permite la creación de un árbol binario de búsqueda mediante la previa creación de nodos que poseen tres campos: valor, variable de tipo entero que almacena al número, izq, variable de tipo Nodo que apunta al nodo en el lado izquierdo y der, que también es de tipo Nodo, pero apunta hacia el nodo derecho. Primeramente, se debe considerar que el programa funciona con un menú de 8 opciones, siendo estas:

1. Insertar un número: Esta opción permite crear el nodo, previamente pidiendo el número a ingresar, a través el método *Arbol::crear_nodo(num)* de la clase *Arbol*. Si el árbol está vacío el nodo creado se insertará como raíz usando el método *Arbol::set_raiz(nuevo)*, que recibe como parámetro el nodo creado anteriormente. A partir de la raíz, los otros nodos irán enlazándose con el método *Arbol::insertar_nodo(raiz, nuevo)*, el que recibe como parámetros la raíz del árbol y el nuevo nodo. Además, el insertar el nodo se verifica que ningún otro nodo posea el mismo número en *valor*.

2. Eliminar un número buscado: Para eliminar un nodo se debe obtener el número específico que se desea eliminar. Luego, se usa el método *Arbol::eliminar_nodo(raiz, num)*, el que recibe como parámetro la raíz del árbol y el número que se debe buscar en los nodos. Si encuentra una coincidencia entre el número con el valor de algún nodo, entonces se revisan diferentes casos. El caso (1) evalúa si el nodo corresponde a la raíz y no apunta a ningún nodo, de manera que la raíz se convierte en NULL, o apunta al menos a uno, donde la raíz se actualiza por ese nodo. Si no cumple con la primera condición, desde este punto el nodo no es la raíz o bien es la raíz, pero apunta a dos nodos. Luego, en el caso (2) el nodo no apunta a nada por la derecha, por lo que el nodo se actualiza por la rama izquierda. Seguidamente, en el caso (3) el nodo apunta a un nodo por la derecha y apunta a NULL por la izquierda, por lo que el nodo es ahora el nodo a la derecha. Finalmente, si el nodo no cumple con ninguna condición, se tiene el caso (4) donde el nodo apunta a dos nodos y se ordena el árbol de manera que no se violen las reglas (se revisa nodo de la izquierda, a partir del que se van revisando los nodos a su derecha y así ordenarlos en orden inverso).

3. Modificar un elemento buscado: Para esta opción se pide el número a modificar. Luego se el método *Arbol::buscar_nodo(raiz, num_objetivo)* recorre el árbol para buscar el nodo que debe modificarse y, si la búsqueda tiene éxito, se guarda en el atributo *otro* de la clase Árbol y se pide el número por el que se va a reemplazar. Después se usa el método *Arbol::modificar_nodo(raiz, otro, num_cambio)* que evalúa si el número de cambio sigue manteniendo las reglas establecidas al comienzo.

4. Mostrar contenido en preorden: Este algoritmo permite mostrar primero la raíz, luego recorre el nodo izquierdo de cada nodo, el nodo mismo y finalmente el nodo derecho. Se realiza con el método *Arbol::preorden(Nodo *tmp)**.

5. Mostrar contenido en inorden: Permite mostrar el nodo izquierdo, luego la raíz y por último el nodo derecho. Básicamente, el orden que sigue es desde el nodo izquierdo en el último nivel hacia la raíz, revisando en cada nodo primero el nodo menor, el mismo nodo, luego el nodo mayor; llega a la raíz y repite el mismo proceso con la rama derecha. Se realiza con el método *Arbol::inorden(Nodo *tmp)**.

6. Mostrar contenido en posorden: Este método de orden muestra el nodo izquierdo, el nodo derecho y después la raíz. Se realiza con el método *Arbol::posorden(Nodo *tmp)**.

7. Generar grafo: En penúltimo lugar, el método de generar grafo permite generar una visualización del árbol binario de búsqueda, creando una instancia a la clase Grafo. Esta recibe como parámetro el nodo raíz, para lo que después genera un archivo .txt en el que se escribe la relación existente entre los nodos. Luego, a partir de ese archivo se genera un .png que permite finalmente observar el ABB.

8. Salir.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

3. Es necesario contar contar con la herramienta _graphviz_ para generar el grafo. Ejecutar las siguientes líneas para su instalación:
* sudo apt install graphviz

4. En añadidura, también debe contarse con Eye of GNOME (eog), por lo que debe ejecutarse:
* sudo apt install eog

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Arbol.cpp Grafo.cpp

Luego, para ejecutar el programa se debe utilizar:
* ./programa

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.
* Graphviz: Herramienta que permite diseñar diagramas.
* Eye of GNOME: Visor de imágenes en entorno GNOME.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0
* Graphviz 2.42.2-3build2
* Eye of GNOME 3.36.3-0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U2/Guia4

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* Para entender bloque switch: https://www.tutorialspoint.com/cplusplus/cpp_switch_statement.htm
* Comprender mejor el concepto de ABB: https://www.youtube.com/watch?v=mtvbVLK5xDQ
* Ayuda para escribir en el archivo .txt de la clase Grafo: https://stackoverflow.com/questions/4211043/write-quotes-to-a-text-file-using-ofstream-c
* A Alejandro Valdés por material base: https://lms.educandus.cl/mod/lesson/view.php?id=730993
