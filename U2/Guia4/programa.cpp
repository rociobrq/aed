#include <iostream>
// #include <string>
using namespace std;

// Clases incluidas.
#include "Arbol.h"
#include "Grafo.h"

int menu(){
    int opc;

    while(true){
        cout << "\n---MENÚ---" << endl;
        cout << "[1] Insertar número" << endl;
        cout << "[2] Eliminar número buscado" << endl;
        cout << "[3] Modificar un elemento buscado" << endl;
        cout << "[4] Mostrar contenido en Preorden" << endl;
        cout << "[5] Mostrar contenido en Inorden" << endl;
        cout << "[6] Mostrar contenido en Posorden" << endl;
        cout << "[7] Generar grafo" << endl;
        cout << "[8] Salir" << endl;

        cout << "\nIngrese la opción: ";

        if(cin >> opc){
            // Al obtener un ingreso correcto, rompe el ciclo.
            break;
        }
        else{
            cout << "Ha ocurrido un error. Intente nuevamente." << endl;
            cin.clear();
            cin.ignore();
        }
    }
    // Se retorna la opción.
    return opc;
}

int obtener_num(string mensaje){
    int num;

    // Recepción de un número entero.
    while(true){
        cout << mensaje;
        if(cin >> num){
            // Si el número ingresado es entero, se rompe el ciclo.
            break;
        }
        else{
            cout << "El número es inválido. Intente nuevamente..." << endl;
            cin.clear();
            cin.ignore();
        }
    }

    // retorno del número.
    return num;
}

int main(){
    Arbol *arbol = new Arbol();
    int opc, num, num_objetivo, num_cambio;
    Nodo *raiz, *otro, *nuevo;
    string mensaje;

    do{
        opc = menu();
        // En cada iteración se actualiza la raíz.
        raiz = arbol->get_raiz();

        switch(opc){
            case 1:
                cout << "\n---INSERTAR NODO---" << endl;

                // Se pide número a ingresar y se recibe el nodo creado.
                mensaje = "Número a insertar: ";
                num = obtener_num(mensaje);
                nuevo = arbol->crear_nodo(num);

                // Si el árbol no posee ningún nodo, el nodo se añade la raíz.
                if(raiz == NULL){
                    arbol->set_raiz(nuevo);
                }
                else{
                    arbol->insertar_nodo(raiz, nuevo);
                }
                break;

            case 2:
                if(raiz != NULL){
                    cout << "\n---ELIMINAR NODO---" << endl;

                    // Se pide número a eliminar y se llama al método.
                    mensaje = "Número a eliminar: ";
                    num = obtener_num(mensaje);
                    arbol->eliminar_nodo(raiz, num);
                }
                else{
                    cout << "El árbol no posee nodos para eliminar." << endl;
                }
                break;

            case 3:
                if(raiz != NULL){
                    cout << "\n---MODIFICAR ELEMENTO BUSCADO---" << endl;

                    // Si árbol posee nodos, se pide número a buscar.
                    mensaje = "Número a modificar: ";
                    num_objetivo = obtener_num(mensaje);

                    // Se llama a método para buscar el nodo a modificar.
                    arbol->buscar_nodo(raiz, num_objetivo);

                    // Recuperar nodo objetivo para aplicar modificación.
                    otro = arbol->get_otro();
                    
                    if(otro != NULL){
                        // Se pide número por el que se desea modificar.
                        mensaje = "Número nuevo: ";
                        num_cambio = obtener_num(mensaje);

                        arbol->modificar_nodo(raiz, otro, num_cambio);
                    }
                }
                else{
                    cout << "El árbol está vacío. " << endl;
                }
                break;

            case 4:
                cout << "\n---IMPRIMIR PREORDEN---" << endl;
                arbol->preorden(raiz);
                cout << "\n";
                break;

            case 5:
                cout << "\n---IMPRIMIR INORDEN---" << endl;
                arbol->inorden(raiz);
                cout << "\n";
                break;

            case 6:
                cout << "\n---IMPRIMIR POSORDEN---" << endl;
                arbol->posorden(raiz);
                cout << "\n";
                break;

            case 7:
                if(raiz != NULL){
                    cout << "\n---GENERAR GRAFO---" << endl;

                    // Se genera instancia a la clase Grafo.
                    Grafo *grafo = new Grafo(raiz);
                }
                else{
                    cout << "Grafo no puede generarse: árbol vacío." << endl;
                }
                break;
        }
    } while(opc != 8);

    return 0;
}
