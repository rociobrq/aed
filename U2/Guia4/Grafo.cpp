#include <iostream>
#include <fstream>
using namespace std;

// Definición de clase.
#include "Grafo.h"

Grafo::Grafo(Nodo* raiz){
    // Permite operar sobre un archivo.
    ofstream archivo;

    // Se abre el archivo en el que se guardará la información.
    archivo.open("grafo.txt");

    // Header archivo de texto.
    archivo << "digraph G {" << endl;
    archivo << "node [style=filled fillcolor=blue];" << endl;

    // Recorre el árbol para escribir en el archivo las relaciones entre nodos.
    recorrer_arbol(raiz, archivo);

    archivo << "}" << endl;

    // Se cierra el archivo después del recorrido.
    archivo.close();

    // Generación del grafo en base a una estructura ABB alojada en el archivo.
    system("dot -Tpng -ografo.png grafo.txt &");

    // Permite visualizar el grafo en formato png usando programa eog.
    system("eog grafo.png &");
}

void Grafo::recorrer_arbol(Nodo *tmp, ofstream &archivo) {
    // Se recorre árbol en preorden y se van agregando los datos al archivo.
    string cadena = "\0";
    string raiz, izq, der;

    if(tmp!= NULL){
        raiz = to_string(tmp->valor);

        if(tmp->izq != NULL){
            // Nodo apunta a otro nodo por la izquierda.
            // Se escribe relación entre el nodo y su rama izquierda.
            izq = to_string(tmp->izq->valor);
            archivo << raiz << "->" << izq << ";" << endl;
        }
        else{
            // No apunta a un nodo en el lado izquierdo (apunta a NULL).
            // Se escribe que nodo apunta a NULL por la izquierda (i).
            cadena = raiz + "i";
            archivo << "\"" << cadena << "\" [shape=point];" << endl;
            archivo << raiz << "->" << "\"" << cadena << "\";" << endl;
        }

        if(tmp->der != NULL){
            // Se escribe relación entre nodo con la rama derecha.
            der = to_string(tmp->der->valor);
            archivo << raiz << "->" << der << ";" << endl;

        }
        else{
            // Si no posee nodos en el lado derecho (d), se apunta a NULL.
            cadena = raiz + "d";
            archivo << "\"" << cadena << "\" [shape=point];" << endl;
            archivo << raiz << "->" << "\"" << cadena << "\";" << endl;
        }

        // Llamada recursiva hasta recorrer cada nodo.
        recorrer_arbol(tmp->izq, archivo);
        recorrer_arbol(tmp->der, archivo);
    }
}
