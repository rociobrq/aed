#ifndef NODO_H
#define NODO_H

// Estructura del nodo.
typedef struct _Nodo {
    int valor;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

#endif
