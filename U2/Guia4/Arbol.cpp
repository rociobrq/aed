#include <iostream>
using namespace std;

#include "Arbol.h"

Arbol::Arbol(){}

void Arbol::set_raiz(Nodo *nodo){
    // Se coloca la raíz del árbol.
    this->raiz = nodo;
    cout << "Ingreso exitoso." << endl;
}

Nodo* Arbol::crear_nodo(int valor){
    // Se genera el nuevo nodo.
    Nodo *nuevo = new Nodo;

    // Su valor es el número y apunta a NULL tanto a izq como a der.
    nuevo->valor = valor;
    nuevo->izq = NULL;
    nuevo->der = NULL;

    return nuevo;
}

void Arbol::insertar_nodo(Nodo *tmp, Nodo *nuevo){
    // Nodo a insertar es menor que el nodo tmp actualmente apuntado.
    if(nuevo->valor < tmp->valor){
        if(tmp->izq == NULL){
            // Si tmp no tiene nodo por la izquierda, se apunta al nuevo nodo.
            tmp->izq = nuevo;
            cout << "Ingreso exitoso." << endl;
        }
        else{
            // Si tmp ya apunta a otro nodo por la izquierda, entonces se
            // llama al método con el nodo a su izquierda.
            insertar_nodo(tmp->izq, nuevo);
        }
    }
    // Nodo a insertar es mayor que el nodo tmp actualmente apuntado.
    else if(nuevo->valor > tmp->valor){
        if(tmp->der == NULL){
            // Si tmp no apunta a nada por la derecha, se apunta al nuevo nodo.
            tmp->der = nuevo;
            cout << "Ingreso exitoso." << endl;
        }
        else{
            // Si tmp ya apunta a otro nodo por la derecha, entonces se
            // llama al método con el nodo apuntando a la derecha.
            insertar_nodo(tmp->der, nuevo);
        }
    }
    else{
        cout << "El nodo con ese valor ya se encuentra en el árbol." << endl;
    }
}

void Arbol::eliminar_nodo(Nodo *&tmp, int valor){
    if(tmp != NULL){
        if(valor < tmp->valor){
            // Se vuelve a llamar al método con el nodo izquierdo de tmp.
            eliminar_nodo(tmp->izq, valor);
        }
        else{
            if(valor > tmp->valor){
                // Se llama el método con nodo derecho de tmp.
                eliminar_nodo(tmp->der, valor);
            }
            else{
                // Se encuentra el nodo a eliminar.
                this->otro = tmp;

                // Es raíz y si al menos uno de sus nodos apunta a NULL.
                if(tmp == this->raiz &&(tmp->der == NULL || tmp->izq == NULL)){
                    if(tmp->izq == NULL && tmp->der == NULL){
                        // Si no apunta a ningún nodo, la raiz será NULL.
                        this->raiz = NULL;
                    }
                    else if(tmp->izq == NULL){
                        // Si no apunta a nada por la izquierda, significa
                        // que apunta a un nodo por la derecha. Por ende, este
                        // es la nueva raíz.
                        this->raiz = tmp->der;
                    }
                    else if(tmp->der == NULL){
                        // No apunta a nada por la derecha. Nodo izquierdo es
                        // nueva raíz.
                        this->raiz = tmp->izq;
                    }
                }
                // Se revisa si el nodo objetivo tiene hijos por la derecha.
                else if(tmp->der == NULL){
                    // Al no tener hijos por la derecha, tmp apunta a su
                    // rama izquierda para enlazar el árbol con esta.
                    tmp = tmp->izq;
                }
                else{
                    // Al tener hijos por la derecha, se revisa si tiene hijos
                    // por la izquierda.
                    if(tmp->izq == NULL){
                        // Si no tiene hijos por la izquierda, entonces tmp
                        // apunta a su rama derecha.
                        tmp = tmp->der;
                    }
                    else{
                        // Nodo tiene hijos en ambas direcciones.
                        Nodo *aux1 = tmp->izq;
                        Nodo *aux2;
                        bool band = false;

                        while(aux1->der != NULL){
                            // Se repite mientras queden nodos en rama derecha.
                            aux2 = aux1;
                            aux1 = aux1->der;
                            band = true;
                        }

                        // Valor de nodo a eliminar se sustituye.
                        tmp->valor = aux1->valor;
                        this->otro = aux1;

                        if(band == true){
                            // Si el ciclo se ha realizado, entonces el nodo
                            // objetivo apunta a rama izquierda del mismo.
                            aux2->der = aux1->izq;
                        }
                        else{
                            tmp->izq = aux1->izq;
                        }
                    }
                }

                cout << "'" << valor << "' eliminado." << endl;
                this->otro = NULL;
            }
        }
    }
    else{
        cout << "No se ha encontrado el valor a eliminar. " << endl;
    }
}

void Arbol::buscar_nodo(Nodo *tmp, int objetivo){
    this->otro = NULL;

    if(objetivo < tmp->valor){
        // Valor menor que el nodo actual y ausencia de nodo en la posición
        // izquierda indican que no se ha encontrado nodo.
        if(tmp->izq == NULL){
            cout << "Sin coincidencias con el valor: " << objetivo << endl;
        }
        else{
            // Si existe un nodo en la izquierda, se vuelve a llamar método.
            this->otro = tmp;
            buscar_nodo(tmp->izq, objetivo);
        }
    }
    else{
        if(objetivo > tmp->valor){
            // Valor mayor que el nodo actual y ausencia de nodo en la posición
            // derecha indican que no ha habido éxito en la búsqueda.
            if(tmp->der == NULL){
                cout << "Sin coincidencias con el valor: " << objetivo << endl;
            }
            else{
                // Si hay un nodo en el lado derecho, se llama al método.
                this->otro = tmp;
                buscar_nodo(tmp->der, objetivo);
            }
        }
        else{
            cout << "El valor " << objetivo << " ha sido localizado. " << endl;
            this->otro = tmp;
        }
    }
}

void Arbol::modificar_nodo(Nodo *tmp, Nodo *objetivo, int num){
    if(tmp->valor == objetivo->valor){
        // Se comprueba que si nodo izquierdo existe, num sea mayor que aquel.
        if((tmp->izq != NULL && num > tmp->izq->valor) || tmp->izq == NULL){
            // Se comprueba que si nodo derecho existe, el num sea menor.
            if((tmp->der != NULL && num < tmp->der->valor) || tmp->der == NULL){

                if(tmp == this->raiz){
                    tmp->valor = num;
                    cout << "Modificación exitosa." << endl;
                }
                else{
                    // Al no ser raíz, se debe corroborar que num
                    // sea de acuerdo al antecesor.

                    if(this->otro->valor < tmp->valor){
                        // Actual es mayor que el antecesor.
                        if(this->otro->valor < num){
                            // Si el número nuevo cumple, se modifica.
                            tmp->valor = num;
                            cout << "Modificación exitosa." << endl;
                        }
                        else{
                            cout << "'" << num << "' no es mayor que -> ";
                            cout << this->otro->valor << endl;
                        }
                    }
                    else{
                        // Actual es menor que el antecesor.
                        if(this->otro->valor > num){
                            // Si el número nuevo cumple, se modifica.
                            tmp->valor = num;
                            cout << "Modificación exitosa." << endl;
                        }
                        else{
                            cout << "'" << num << "' no es menor que -> ";
                            cout << this->otro->valor << endl;
                        }
                    }
                }
            }
            else{
                cout << "'" << num << "' no es menor que -> ";
                cout << tmp->der->valor << endl;
            }
        }
        else{
            cout << "'" << num << "' no es mayor que -> ";
            cout << tmp->izq->valor << endl;
        }
    }
    else{
        // Búsqueda del nodo objetivo (puede ser mayor o menor que el actual).
        if(objetivo->valor > tmp->valor){
            // Guardar nodo actual.
            this->otro = tmp;
            // Si es mayor se llama al método con el nodo a la derecho.
            if(tmp->der != NULL){
                modificar_nodo(tmp->der, objetivo, num);
            }
        }
        else{
            // Guardar nodo actual.
            this->otro = tmp;
            // Si es menor se llama al método con el nodo a la izquierda.
            if(tmp->izq != NULL){
                modificar_nodo(tmp->izq, objetivo, num);
            }
        }
    }
}

void Arbol::preorden(Nodo *tmp){
    if(tmp != NULL){
        // Se imprime  raíz.
        cout << tmp->valor  << " | ";
        // Nodo a la izquierda.
        preorden(tmp->izq);
        // Nodo a la derecha.
        preorden(tmp->der);
    }
}

void Arbol::inorden(Nodo *tmp){
    if(tmp != NULL){
        // Nodo a la izquierda.
        inorden(tmp->izq);
        // Se imprime raíz.
        cout << tmp->valor  << " | ";
        // Nodo en la rama derecha.
        inorden(tmp->der);
    }
}

void Arbol::posorden(Nodo *tmp){
    if(tmp != NULL){
        // Se recorre nodo a la izquierda.
        posorden(tmp->izq);
        // Nodo a la derecha.
        posorden(tmp->der);
        // Se imprime raíz.
        cout << tmp->valor  << " | ";
    }
}

Nodo* Arbol::get_raiz(){
    return this->raiz;
}

Nodo* Arbol::get_otro(){
    return this->otro;
}
