#ifndef ARBOL_H
#define ARBOL_H

#include <iostream>
using namespace std;

// Definición de nodo.
#include "Nodo.h"

class Arbol{
    private:
        // Apunta a la raíz del árbol.
        Nodo *raiz = NULL;
        // Auxiliar para eliminar y modificar un nodo.
        Nodo *otro = NULL;

    public:
        // Constructor.
        Arbol();

        // Métodos.
        void set_raiz(Nodo *nodo);
        Nodo* crear_nodo(int valor);
        void insertar_nodo(Nodo *tmp, Nodo *nuevo);
        void eliminar_nodo(Nodo *&tmp, int valor);
        void buscar_nodo(Nodo *tmp, int objetivo);
        void modificar_nodo(Nodo *tmp, Nodo *objetivo, int valor);
        void preorden(Nodo *apnodo);
        void inorden(Nodo *apnodo);
        void posorden(Nodo *apnodo);
        Nodo* get_raiz();
        Nodo* get_otro();
};
#endif
