#ifndef ARBOL_H
#define ARBOL_H

#include <iostream>
#include <stdlib.h>
using namespace std;

/* Definición de nodo */
#include "Nodo.h"

class Arbol{
    private:
        Nodo *raiz = NULL;
        Nodo *tmp = NULL;

    public:
        /* Constructor */
        Arbol();

        /* Métodos */
        void set_raiz(Nodo* raiz);
        Nodo* crear_nodo(string id);
        void insercion_balanceado(Nodo **nodocabeza, bool *bo, string info);
        void busqueda(Nodo *nodo, string info);
        void modificar(Nodo *nodo, Nodo *objetivo, string nuevo, int c);
        void restructura1(Nodo **nodocabeza, bool *bo);
        void restructura2(Nodo **nodocabeza, bool *bo);
        void borra(Nodo **aux1, Nodo **otro1, bool *bo);
        void eliminacion_balanceado(Nodo **nodocabeza, bool *bo, string infor);
        Nodo* get_tmp();
};
#endif
