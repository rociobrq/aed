# Guía 5: AVL

El programa permite crear un árbol balanceado (AVL), estructura que se asemeja a un árbol binario de búsqueda (ABB); sin embargo, un AVL tiene un mecanismo balanceado de inserción y de eliminación. De manera resumida, en un AVL un nodo cuenta con un factor de equilibrio (fe), un string para la información y puede apuntar a otro nodo tanto a la derecha como a la izquierda. El factor de equilibrio se relaciona con la rama izquierda y derecha de un nodo; por ende, para obtener este valor es necesario calcular la diferencia de nivel entre la rama derecha y la rama izquierda. Por ejemplo, el factor de equilibrio de un nodo será 0 si ambas ramas cuentan con los mismos niveles o si el nodo no presenta hijos. En esta línea, se considera que el árbol está balanceado si todos los nodos tienen un factor de equilibrio -1, 0 o 1, de manera que los métodos de inserción y eliminación responden a algoritmos que se encargan de balancear el árbol para respetar aquella condición. Luego, en el programa se pueden identificar distintas opciones, tales como:

1. Cargar datos de archivo (170K): Se insertan más de 170000 mil nodos desde un archivo.

2. Cargar datos de archivo (500): Se insertan 500 nodos desde un archivo.

3. Insertar: Se distinguen dos escenarios, donde se añade a la izquierda o a la derecha, usando la recursividad para hallar la posición. Al añadirse en la izquierda (string menor), se busca la posición del nodo a través de un llamado recursivo a la función Arbol::insercion_balanceado(Nodo **nodocabeza, bool *bo, string info), pasando el nodo izquierdo por referencia en cada ocasión. Al detectar que el nodo izquierdo no existe, se entiende que en aquella posición se debe agregar el nuevo nodo, por lo que se procede a crear la estructura. Luego de creado el nodo, la referencia se actualiza y el nodo a la izquierda es distinto de NULL. En este punto, se distinguen tres casos: si la derecha del nodo raíz tiene un nivel mayor que la izquierda (fe igual a +1), entonces ambas ramas quedan con el mismo nivel tras añadir el nodo nuevo; si ambos lados poseen un mismo nivel  (fe igual a 0), el que se añada uno a la izquierda significa que el nodo raíz toma un fe igual a -1; por último, si la rama izquierda es un nivel mayor que la derecha (fe igual a -1), se debe hacer una reestructuración con rotación II o ID. Al añadirse uno a la derecha (string mayor) se cumple la misma dinámica, con la diferencia de que el llamado recursivo a la función Arbol::insercion_balanceado(Nodo **nodocabeza, bool *bo, string info) es con el nodo derecho y que se distinguen tres casos distintos: si la rama izquierda del nodo raíz es mayor en una unidad que la derecha (fe igual a -1), el que se añada uno a la derecha permite que ambos lados queden con los mismos niveles; si ambas ramas tienen mismo nivel, entonces el nodo queda con fe igual a +1 al añadir la nueva estructura; por último, si la derecha es de un nivel mayor que la izquierda, añadir otro nodo a la derecha desbalancea el árbol y se debe hacer reestructuración con rotación DD o DI.

4. Eliminar: Para eliminar un nodo, se pide el id y luego se llama al método eliminacion_balanceado(Nodo **nodocabeza, bool *bo, string infor), el cual va revisando por las ramas si algún nodo contiene en su string la id buscada. Al encontrarla, se distinguen cuatro casos: nodo con dos hijos, nodo con un hijo a la izquierda, nodo con un hijo a la derecha y nodo sin hijos.

5. Modificar: Se pide el id a modificar y luego se el método Arbol::busqueda(Nodo *nodo, string info), el cual recorre el árbol para buscar el nodo que debe modificarse y, si la búsqueda tiene éxito, se guarda en el atributo *tmp* de la clase Árbol. Después de encontrar el nodo, se pide el nuevo id por el que se va a reemplazar. Después se usa el método Arbol::modificar(Nodo *nodo, Nodo *objetivo, string info, int c), el cual evalúa si el id de cambio sigue manteniendo las reglas establecidas al comienzo (string mayor que el string en el nodo antecesor, mayor a rama izquierda y menor a rama derecha).

6. Generar grafo: Esta opción permite generar el grafo sin hacer ninguna alteración del árbol, para lo que genera una instancia de la clase Grafo.

## Pre-requisitos
* Sistema operativo Ubuntu versión 18.04 o superior
* G++

## Instalación
1. La versión del sistema operativo puede conocerse con el comando:
* lsb_release -a

Si la versión es inferior, se puede actualizar usando:
* sudo apt upgrade

Finalmente, se opta por reiniciar con:
* sudo reboot

2. Con la finalidad de instalar el compilador G++ para C++, se deben ejecutar las siguientes líneas:
* sudo apt instal g++
* sudo apt install build-essential

3. Es necesario contar contar con la herramienta _graphviz_ para generar el grafo. Ejecutar las siguientes líneas para su instalación:
* sudo apt install graphviz

4. En añadidura, también debe contarse con Eye of GNOME (eog), por lo que debe ejecutarse:
* sudo apt install eog

## Ejecutando las pruebas
Para compilar el programa es necesario usar el siguiente comando:
* make

Por otro lado, también se puede emplear:
* g++ programa.cpp Arbol.cpp Grafo.cpp

Luego, para ejecutar el programa se debe utilizar:
* ./programa

## Construido con
* Ubuntu: Sistema operativo.
* C++: Lenguaje de programación.
* Atom: Editor de código.
* G++: Compilador de C++.
* Graphviz: Herramienta que permite diseñar diagramas.
* Eye of GNOME: Visor de imágenes en entorno GNOME.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* C++ 11
* G++ 9.3.0
* Atom 1.46.0
* Graphviz 2.42.2-3build2
* Eye of GNOME 3.36.3-0

#### Versiones del desarrollo del código:
https://gitlab.com/rociobrq/aed/-/tree/master/U2/Guia5

## Autor
* Rocío Rodríguez - Desarrollo del código y narración README.

## Expresiones de gratitud
* https://stackoverflow.com/questions/42951748/c-bst-tree-insert-a-string
* https://www.tutorialspoint.com/how-to-read-a-text-file-with-cplusplus
* https://www.geeksforgeeks.org/vector-in-cpp-stl/
* https://www.geeksforgeeks.org/stdstringcompare-in-c/
* https://www.programiz.com/cpp-programming/library-function/cstdlib/free
