#include <iostream>
#include <fstream>
using namespace std;

// Definición de clase.
#include "Grafo.h"

Grafo::Grafo(Nodo* raiz){
    /* Permite operar sobre un archivo */
    ofstream archivo;

    /* Se abre el archivo en el que se guardará la información */
    archivo.open("grafo.txt");

    /* Header archivo de texto */
    archivo << "digraph G {" << endl;
    archivo << "node [style=filled fillcolor=blue];" << endl;

    archivo << "nullraiz [shape=point];" << endl;
    archivo << "nullraiz->\"" << raiz->id << "\" [label=";
    archivo << raiz->fe << "];" << endl;

    /* Recorre el árbol para escribir en el archivo las relaciones entre nodos */
    recorrer_arbol(raiz, archivo);

    archivo << "}" << endl;

    /* Se cierra el archivo después del recorrido */
    archivo.close();

    /*Generación del grafo en base a una estructura ABB alojada en el archivo */
    system("dot -Tpng -Gdpi=100 -ografo.png grafo.txt &");

    /* Permite visualizar el grafo en formato png usando programa eog */
    system("eog grafo.png &");
}

void Grafo::recorrer_arbol(Nodo *tmp, ofstream &archivo) {
    /* Se recorre árbol en preorden y se van agregando los datos al archivo */
    string cadena = "\0";

    if(tmp!= NULL){
        if(tmp->izq != NULL){
            /* Nodo apunta a otro nodo por la izquierda.
            Se escribe relación entre el nodo y su rama izquierda */
            archivo << "\"" << tmp->id << "\"->\"" << tmp->izq->id;
            archivo << "\" [label=\"" << tmp->izq->fe << "\"];" << endl;
        }
        else{
            /* No apunta a un nodo en el lado izquierdo (apunta a NULL).
            Se escribe que nodo apunta a NULL por la izquierda (i) */
            cadena = tmp->id + "i";
            archivo << "\"" << cadena << "\" [shape=point]; " << endl;
            archivo << "\"" << tmp->id << "\"->" << "\"" << cadena << "\";" << endl;
        }

        if(tmp->der != NULL){
            /* Se escribe relación entre nodo con la rama derecha */
            archivo << "\"" << tmp->id << "\"->\"" << tmp->der->id;
            archivo << "\" [label=\"" << tmp->der->fe << "\"];" << endl;
        }
        else{
            /* Si no posee nodos en el lado derecho (d), se apunta a NULL */
            cadena = tmp->id + "d";
            archivo << "\"" << cadena << "\" [shape=point];" << endl;
            archivo << "\"" << tmp->id << "\"->" << "\"" << cadena << "\";" << endl;
        }

        /* Llamada recursiva hasta recorrer cada nodo */
        recorrer_arbol(tmp->izq, archivo);
        recorrer_arbol(tmp->der, archivo);
    }
}
