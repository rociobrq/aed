#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
#include <fstream>
using namespace std;

/* Definición de nodo */
#include "Nodo.h"

class Grafo{
    public:
        /* Constructor recibe nodo raíz del árbol */
        Grafo(Nodo* raiz);

        /* Método recorre árbol para escribir en el archivo txt */
        void recorrer_arbol(Nodo *tmp, ofstream &archivo);
};
#endif
