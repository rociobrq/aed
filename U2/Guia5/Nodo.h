#ifndef NODO_H
#define NODO_H

/* Estructura del nodo */
typedef struct _Nodo {
    string id ="\0";
    int fe = 0;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

#endif
