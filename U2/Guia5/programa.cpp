#include <iostream>
using namespace std;

/* Clases incluidas */
#include "Arbol.h"
#include "Grafo.h"

int menu(){
    int op;
    do{
        cout << "\n--------------------\n" << endl;
        cout << "[1] Cargar datos de archivo (170K)" << endl;
        cout << "[2] Cargar datos de archivo (500)" << endl;
        cout << "[3] Insertar" << endl;
        cout << "[4] Eliminar" << endl;
        cout << "[5] Modificar ID buscado" << endl;
        cout << "[6] Grafo" << endl;
        cout << "[0] Salir" << endl;
        cout << "Opción: ";
        cin >> op;

        if(cin.fail()){
            cout << "Ha ocurrido un error. " << endl;
        }
    } while(op < 0 || op > 6);

    return op;
}

string obtener_id(string mensaje){
    string input;

    cout << mensaje;
    cin >> input;
    for(int i = 0; i < input.size(); i++){
        input[i] = toupper(input[i]);
    }

    return input;
}

/* Función principal */
int main(){
    Arbol *arbol = new Arbol();
    int opcion;
    string elemento;
    string elemento1;
    bool inicio;
    string mensaje;
    Nodo *raiz = NULL;
    Nodo *aux = NULL;
    Grafo *grafo;
    fstream archivo;
    string tmp;

    system("clear");
    opcion = menu();

    while (opcion){
        switch(opcion){
            case 1:
                /* Archivo contiene más de 170000 ids */
                archivo.open("pdb_id.txt", ios::in);

                /* Lectura de cada línea */
                while(getline(archivo, tmp)){
                    inicio = false;
                    /* Se inserta cada línea */
                    arbol->insercion_balanceado(&raiz, &inicio, tmp);
                }

                archivo.close();
                /* Generación grafo */
               grafo = new Grafo(raiz);
                break;

            case 2:
                /* Archivo contiene 500 ids */
                archivo.open("500.txt", ios::in);

                /* Lectura de cada línea */
                while(getline(archivo, tmp)){
                    inicio = false;
                    arbol->insercion_balanceado(&raiz, &inicio, tmp);
                }

                archivo.close();
                /* Generación grafo */
               grafo = new Grafo(raiz);
                break;

            case 3:
                /* Se obtiene id a ingresar para insertar el nodo */
                mensaje = "Ingresar ID: ";
                elemento = obtener_id(mensaje);
                inicio = false;
                /* Llamada al método de inserción */
                arbol->insercion_balanceado(&raiz, &inicio, elemento);

                /* Generación del grafo */
               grafo = new Grafo(raiz);
                break;

            case 4:
                if(raiz != NULL){
                    /* Se obtiene id del nodo a eliminar */
                    mensaje = "Eliminar elemento: ";
                    elemento = obtener_id(mensaje);
                    inicio = false;

                    /* Llamada al método de eliminación */
                    arbol->eliminacion_balanceado(&raiz, &inicio, elemento);

                    /* Generación del grafo */
                    if(raiz != NULL){
                        grafo = new Grafo(raiz);
                    }
                    else{
                        cout << "Se ha eliminado la raíz del árbol." << endl;
                    }
                }
                else{
                    cout << "El árbol está vacío... " << endl;
                }
                break;

            case 5:
                if(raiz != NULL){
                    /* Se obtiene id del nodo a buscar */
                    mensaje = "Buscar elemento: ";
                    elemento = obtener_id(mensaje);

                    /* Llamada al método de búsqueda y obtención del nodo*/
                    arbol->busqueda(raiz, elemento);
                    aux = arbol->get_tmp();

                    if(aux != NULL){
                        /* Si se ha encontrado el nodo, se pide id nueva */
                        mensaje = "Modificar por: ";
                        elemento1 = obtener_id(mensaje);

                        /* Revisa si valor original es igual al valor nuevo */
                        if(elemento == elemento1){
                            cout << "Valor inalterado." << endl;
                        }
                        else{
                            /* Si el valor es distinto, se llama a método para
                            ejecutar la modificación */
                            arbol->modificar(raiz, aux, elemento1, 0);

                            /* Generación del grafo */
                           grafo = new Grafo(raiz);
                        }
                    }
                }
                else{
                    cout << "El arbol está vacío..." << endl;
                }

                break;

            case 6:
                if(raiz != NULL){
                    cout << "árbol" << endl;
                   grafo = new Grafo(raiz);
                }
                else{
                    cout << "El árbol está vacío..." << endl;
                }
                break;

            case 0:
                exit(0);
        }
        arbol->set_raiz(raiz);
        opcion = menu();
    }

    return 0;
}
