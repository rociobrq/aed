#include <iostream>
#include <stdlib.h>
using namespace std;

/* Definición de clase */
#include "Arbol.h"

Arbol::Arbol(){}

void Arbol::set_raiz(Nodo* raiz){
    // Se inserta raíz del árbol.
    this->raiz = raiz;
}

void Arbol::insercion_balanceado(Nodo **nodocabeza, bool *bo, string info){
    Nodo *nodo = NULL;
    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;

    nodo = *nodocabeza;

    if(nodo != NULL){
        /* Valor es menor que el valor actual, indica ingreso a la izquierda */
        if(info.compare(nodo->id) < 0){
            /* Llamada recursiva pasando nodo izquierdo por referencia */
            insercion_balanceado(&(nodo->izq), bo, info);

            if(*bo == true){
                switch(nodo->fe){
                    case 1:
                        /*Derecha es de un nivel mayor que la izquierda. Tras
                        añadir a la izquierda, ambos tienen igual nivel*/
                        nodo->fe = 0;
                        *bo = false;
                        break;

                    case 0:
                        /* Mismo nivel ambos lados. Tras añadir uno a la
                        izquierda se inclina a ese lado*/
                        nodo->fe = -1;
                        break;

                    case -1:
                        /*Izquierda es de un nivel mayor que la derecha.
                        Desbalance tras añadir uno a la izquierda*/

                        /* Reestructuración */
                        nodo1 = nodo->izq;

                        /* Rotacion II */
                        if(nodo1->fe <= 0){
                            /*Nodo que recibe hijo pasa a apuntar por la derecha
                            a su antecesor y por la izquierda al nuevo nodo*/
                            nodo->izq = nodo1->der;
                            nodo1->der = nodo;
                            nodo->fe = 0;
                            nodo = nodo1;
                        }
                        else {
                            /* Rotacion ID
                            El nuevo nodo pasa a ser el nodo raíz */
                            nodo2 = nodo1->der;
                            nodo->izq = nodo2->der;
                            nodo2->der = nodo;
                            nodo1->der = nodo2->izq;
                            nodo2->izq = nodo1;

                            /* Actuaización de factores de equilibrio */
                            if(nodo2->fe == -1){
                                nodo->fe = 1;
                            }
                            else{
                                nodo->fe =0;
                            }

                            if(nodo2->fe == 1){
                                nodo1->fe = -1;
                            }
                            else{
                                nodo1->fe = 0;
                            }

                            /* Se actualiza nodo raíz por último ingresado */
                            nodo = nodo2;
                        }

                        /* Se actualiza fe del nodo y se termina inserción */
                        nodo->fe = 0;
                        *bo = false;
                        break;
                }
            }
        }
        else {
            /* Valor es mayor que el valor actual, indica ingreso a la derecha */
            if(info.compare(nodo->id) > 0){
                insercion_balanceado(&(nodo->der), bo, info);

                if(*bo == true){
                    switch(nodo->fe) {
                        case -1:
                            /* Izquierda es de un nivel mayor que la derecha.
                            Tras añadir uno de la derecha se equilibra árbol*/
                            nodo->fe = 0;
                            *bo = false;
                            break;

                        case 0:
                            /* Izquierda y derecha tienen mismo nivel */
                            nodo->fe = 1;
                            break;

                        case 1:
                            /*Derecha es de un nivel mayor que la izquierda.
                            Tras añadir a la derecha se desbalancea el árbol*/

                            /* Reestructuración */
                            nodo1 = nodo->der;

                            if (nodo1->fe >= 0) {
                                /* Rotacion DD
                                Nodo que recibe hijo pasa a apuntar por la
                                izquierda a su antecesor y por la derecha al
                                nuevo nodo */
                                nodo->der = nodo1->izq;
                                nodo1->izq = nodo;
                                nodo->fe = 0;
                                nodo = nodo1;

                            } else {
                                /* Rotacion DI
                                El nuevo nodo pasa a ser el nodo raíz, donde
                                apunta por la izquierda a rama menor y por la
                                derecha al nodo antecesor */
                                nodo2 = nodo1->izq;
                                nodo->der = nodo2->izq;
                                nodo2->izq = nodo;
                                nodo1->izq = nodo2->der;
                                nodo2->der = nodo1;

                                /* Actuaización de factores de equilibrio */
                                if (nodo2->fe == 1){
                                    nodo->fe = -1;
                                } else{
                                    nodo->fe = 0;
                                }

                                if (nodo2->fe == -1){
                                    nodo1->fe = 1;
                                } else{
                                    nodo1->fe = 0;
                                }

                                nodo = nodo2;
                            }

                            /* Nodo cabeza */
                            nodo->fe = 0;
                            *bo = false;
                            break;
                    }
                }
            }
            else {
                cout << "El nodo '" << info << "' ya se encuentra en el árbol";
            }
        }
    }
    else {
        /* Creación del nodo insertado */
        nodo = new Nodo;
        nodo->izq = NULL;
        nodo->der = NULL;
        nodo->id = info;
        nodo->fe = 0;
        *bo = true;
    }

    /* Referencia toma valor del nodo creado */
    *nodocabeza = nodo;
}

void Arbol::busqueda(Nodo *nodo, string info){

    if(nodo != NULL){
        /* Si el valor buscado es menor que el actual, se busca por izquierda */
        if(info.compare(nodo->id) < 0){
            busqueda(nodo->izq, info);
        }
        else{
            /* Si el valor buscado es mayor que el actual, se busca por derecha */
            if(info.compare(nodo->id) > 0){
                busqueda(nodo->der, info);
            }
            else{
                /* Valor buscado es igual al actual */
                cout << "El nodo se encuentra en el árbol." << endl;
                this->tmp = nodo;
            }
        }
    }
    else{
        cout << "El nodo no se encuentra en el árbol. " << endl;
    }
}

void Arbol::modificar(Nodo *nodo, Nodo *objetivo, string info, int c){

    if(nodo->id.compare(objetivo->id) == 0){
        /* Se comprueba que si nodo izquierdo existe, id sea mayor que aquel */
        if((nodo->izq != NULL && info.compare(nodo->izq->id) > 0) ||
                nodo->izq == NULL){

            /* Se comprueba que si nodo derecho existe, el id sea menor */
            if((nodo->der != NULL && info.compare(nodo->der->id) < 0) ||
                    nodo->der == NULL){

                if(c == 0){
                    nodo->id = info;
                    cout << "Modificación exitosa." << endl;
                }
                else{
                    /* Al no ser raíz, se debe corroborar que id vaya de
                    acuerdo al antecesor */

                    if(this->tmp->id.compare(nodo->id) < 0){
                        /* Actual es mayor que el antecesor */
                        if(this->tmp->id.compare(info) < 0){
                            /* Si el string nuevo cumple, se modifica */
                            nodo->id = info;
                            cout << "Modificación exitosa." << endl;
                        }
                        else{
                            cout << "'" << info << "' no es mayor que -> ";
                            cout << this->tmp->id << endl;
                        }
                    }
                    else{
                        /* Actual es menor que el antecesor */
                        if(this->tmp->id.compare(info) > 0){
                            /* Si el número nuevo cumple, se modifica */
                            nodo->id = info;
                            cout << "Modificación exitosa." << endl;
                        }
                        else{
                            cout << "'" << info << "' no es menor que -> ";
                            cout << this->tmp->id << endl;
                        }
                    }
                }
            }
            else{
                cout << "'" << info << "' no es menor que -> ";
                cout << nodo->der->id << endl;
            }
        }
        else{
            cout << "'" << info << "' no es mayor que -> ";
            cout << nodo->izq->id << endl;
        }
    }
    else{
        /* Búsqueda del nodo objetivo (puede ser mayor o menor que el actual) */
        if(objetivo->id.compare(nodo->id) > 0){
            /* Guardar nodo actual */
            this->tmp = nodo;
            /* Si es mayor se llama al método con el nodo a la derecho */
            if(nodo->der != NULL){
                modificar(nodo->der, objetivo, info, c + 1);
            }
        }
        else if (objetivo->id.compare(nodo->id) < 0){
            /* Guardar nodo actual */
            this->tmp = nodo;
            /* Si es menor se llama al método con el nodo a la izquierda */
            if(nodo->izq != NULL){
                modificar(nodo->izq, objetivo, info, c + 1);
            }
        }
    }
}

void Arbol::restructura1(Nodo **nodocabeza, bool *bo){
    /* Reestructura tras eliminar nodo a la izquierda o nodo con dos hijos */
    Nodo *nodo, *nodo1, *nodo2;
    nodo = *nodocabeza;

    if(*bo == true){
        switch(nodo->fe){
            case -1:
                /* Al eliminar nodo a la izquierda, ambas ramas quedan con
                el mismo nivel */
                nodo->fe = 0;
                break;

            case 0:
                /* Al eliminar nodo a la izquierda, rama derecha queda un nivel
                mayor que la izquierda */
                nodo->fe = 1;
                *bo = false;
                break;

            case 1:
                /* Reestructuración del árbol */
                nodo1 = nodo->der;

                if(nodo1->fe >= 0){
                    /* Rotación DD */
                    nodo->der = nodo1->izq;
                    nodo1->izq = nodo;

                    switch(nodo1->fe){
                        case 0:
                            nodo->fe = 1;
                            nodo1->fe = -1;
                            *bo = false;
                            break;
                        case 1:
                            nodo->fe = 0;
                            nodo1->fe = 0;
                            *bo = false;
                    }
                    nodo = nodo1;
                }
                else{
                    /* Rotación DI */
                    nodo2 = nodo1->izq;
                    nodo->der = nodo2->izq;
                    nodo2->izq = nodo;
                    nodo1->izq = nodo2->der;
                    nodo2->der = nodo1;

                    if(nodo2->fe = 1){
                        nodo->fe = -1;
                    }
                    else{
                        nodo->fe = 0;
                    }
                    if(nodo2->fe = -1){
                        nodo1->fe = 1;
                    }
                    else{
                        nodo1->fe = 0;
                    }

                    nodo = nodo2;
                    nodo2->fe = 0;
                }
                break;
        }
    }
    /* Se actualiza nodo por referencia */
    *nodocabeza = nodo;
}

void Arbol::restructura2(Nodo **nodocabeza, bool *bo) {
    /* Reestructura tras eliminar un nodo a la derecha */
    Nodo *nodo, *nodo1, *nodo2;
    nodo = *nodocabeza;

    if (*bo == true) {
        switch (nodo->fe) {
            case 1:
                /* Rama derecha con un nivel mayor. Tras eliminar nodo derecho,
                ambas ramas tienen mismo nivel */
                nodo->fe = 0;
                break;

            case 0:
                /* Ambas ramas con mismo nivel. Tras eliminar nodo derecho,
                rama izquierda es superior por una unidad */
                nodo->fe = -1;
                *bo = false;
                break;

            case -1:
                /* Rama izquierda con nivel mayor. Tras eliminar nodo derecho,
                árbol se desbalancea */

                nodo1 = nodo->izq;

                if(nodo1->fe <= 0){
                    /* Rotacion II  */
                    nodo->izq = nodo1->der;
                    nodo1->der = nodo;

                    switch(nodo1->fe){
                        case 0:
                            nodo->fe = -1;
                            nodo1->fe = 1;
                            *bo = false;
                            break;

                        case -1:
                            nodo->fe = 0;
                            nodo1->fe = 0;
                            *bo = false;
                            break;
                    }

                    nodo = nodo1;
                }
                else {
                    /* Rotacion ID */
                    nodo2 = nodo1->der;
                    nodo->izq = nodo2->der;
                    nodo2->der = nodo;
                    nodo1->der = nodo2->izq;
                    nodo2->izq = nodo1;

                    if(nodo2->fe == -1){
                        nodo->fe = 1;
                    }
                    else{
                        nodo->fe = 0;
                    }
                    if (nodo2->fe == 1){
                        nodo1->fe = -1;
                    }
                    else{
                        nodo1->fe = 0;
                    }

                    nodo = nodo2;
                    nodo2->fe = 0;
                }
                break;
        }
    }

    /* Se actualiza nodo por referencia */
    *nodocabeza = nodo;
}

void Arbol::borra(Nodo **aux1, Nodo **otro1, bool *bo) {
    Nodo *aux, *otro;
    /* Rama izquierda */
    aux = *aux1;
    /* Nodo principal */
    otro = *otro1;

    if(aux->der != NULL){
        /* Nodo aux posee nodos a la derecha */
        borra(&(aux->der), &otro, bo);
        restructura2(&aux, bo);
    }
    else{
        /* Si nodo aux no posee nodos a la derecha, el nodo principal se
        actualiza con valor de aux y este último se actualiza por nodo en su
        rama izquierda*/
        otro->id = aux->id;
        aux = aux->izq;
        *bo = true;
    }

    /* Se actualizan nodos por referencia */
    *aux1 = aux;
    *otro1 = otro;
}

void Arbol::eliminacion_balanceado(Nodo **nodocabeza, bool *bo, string infor) {
    Nodo *nodo, *otro;
    nodo = *nodocabeza;

    if (nodo != NULL) {
        /* Búsqueda del nodo dependiendo de su id */
        if (infor.compare(nodo->id) < 0) {
            /*Si el nodo es menor que el nodo actual, se hace llamada recursiva
            con nodo izquierdo*/
            eliminacion_balanceado(&(nodo->izq), bo, infor);
            restructura1(&nodo, bo);
        }

        else {
            if (infor.compare(nodo->id) > 0) {
                /*Si el nodo es mayor que el nodo actual, se hace llamada
                recursiva con nodo derecho*/
                eliminacion_balanceado(&(nodo->der), bo, infor);
                restructura2(&nodo, bo);
            }
            else {
                /* Nodo es encontrado en el árbol y se trabaja como "otro" */
                otro = nodo;

                /* Eliminación de único nodo en el árbol */
                if((otro->der == NULL && otro->izq == NULL) &&
                    this->raiz == otro){
                    nodo = NULL;
                }

                /* Carece de nodos en la rama derecha */
                else if (otro->der == NULL) {
                    /* Pasa a tomar valor de nodo en rama izquierda */
                    nodo = otro->izq;
                    *bo = true;
                }
                else {
                    /* Posee nodos rama derecha, pero carece de nodos en la
                    rama izquierda */
                    if (otro->izq == NULL) {
                        /* Pasa a tomar valor de nodo en rama derecha */
                        nodo = otro->der;
                        *bo = true;
                    }
                    /* Posee nodos en rama izquierda y derecha */
                    else {
                        borra(&(otro->izq), &otro, bo);
                        restructura1(&nodo, bo);
                        otro = NULL;
                    }
                }
            }
        }
    }
    else {
        printf("El nodo NO se encuentra en el árbol\n");
    }

    /* Nodo por referencia se actualiza con nuevo valor */
    *nodocabeza = nodo;
}

Nodo* Arbol::get_tmp(){
    return this->tmp;
}
